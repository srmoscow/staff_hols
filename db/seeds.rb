# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# Environment variables (ENV['...']) are set in the file config/application.yml.
# See http://railsapps.github.com/rails-environment-variables.html
# puts 'ROLES'

User.delete_all

md   = User.create( name: 'Steve Rawlinson',    email: 'steve@tagadab.com',            is_manager: true,  is_admin: true,                  username: 'steve.rawlinson')
anna = User.create( name: 'Anna Scapin',        email: 'anna@tagadab.com',             is_manager: true,  is_admin: true,   manager: md,   username: 'anna.scapin')
psr  = User.create( name: 'Stephen Richards',   email: 'stephen.richards@tagadab.com', is_manager: true,  is_admin: true,   manager: md,   username: 'stephen.richards')
wh   = User.create( name: 'Will Hamilton',      email: 'will@tagadab.com',             is_manager: false, is_admin: false,  manager: psr,  username: 'william.hamilton')
da   = User.create( name: 'Dan Arber',          email: 'daniel@tagadab.com',           is_manager: false, is_admin: false,  manager: psr,  username: 'daniel.arber')
jd   = User.create( name: 'Jiv Dhaliwal',       email: 'jiv@tagadab.com',              is_manager: false, is_admin: false,  manager: psr,  username: 'jiv')
mm   = User.create( name: 'Michael Malet',      email: 'michael.malet@tagadab.com',    is_manager: false, is_admin: false,  manager: psr,  username: 'michael.malet')
tj   = User.create( name: 'Timothy Jarman',     email: 'tim.jarman@tagadab.com',       is_manager: false, is_admin: false,  manager: anna, username: 'tim.jarman')
gp   = User.create( name: 'Giuseppe Patane',    email: 'giuseppe@tagadab.com',         is_manager: false, is_admin: false,  manager: anna, username: 'giuseppe.patane')
nc   = User.create( name: 'Nick Calvert',       email: 'nick.calvert@tagadab.com',     is_manager: false, is_admin: false,  manager: md,   username: 'nick.calvert')
sk   = User.create( name: 'Simon Kirby',        email: 'simon@tagadab.com',            is_manager: false, is_admin: false,  manager: md,   username: 'simon.kirby')
na   = User.create( name: 'Nick Arnold',        email: 'nicka@tagadab.com',            is_manager: false, is_admin: false,  manager: md,   username: 'nick.arnold')

jd = User.create( name: 'Jo Dunlop',        email: 'jo@tagadab.com',            is_manager: false, is_admin: false,  manager: md,   username: 'jo.dunlop')


# test   = User.create( name: 'Test',        email: 'stephen@stephenrichards.eu',           password: 'changeme', password_confirmation: 'changeme', is_manager: false, is_admin: false,  manager: User.find(39))
# test.create_allowance(2013, 25)



HolidayYear.delete_all
hy2012 = HolidayYear.create(year: 2012, description: 'Year Beginning April 2012', start_date: Date.new(2012, 4, 1), end_date: Date.new(2013, 3, 31))
hy2013 = HolidayYear.generate_next_year
hy2014 = HolidayYear.generate_next_year


Allowance.delete_all

md.create_allowance(2013, 25)
anna.create_allowance(2013, 24, 6.5)
psr.create_allowance(2013, 25, 5)
tj.create_allowance(2013, 21, 0)
wh.create_allowance(2013, 22, 1.5)
da.create_allowance(2013, 21, 5)
gp.create_allowance(2013, 20)
jd.create_allowance(2013, 21, 0.5)
mm.create_allowance(2013, 20, 4.5)
nc.create_allowance(2013, 25, 0)
sk.create_allowance(2013, 25, 0)
na.create_allowance(2013, 25, 0)



BookedHoliday.delete_all

BookedHoliday.create!(
      user_id:                  anna.id, 
      holiday_year_id:          hy2013.id, 
      start_date:               '23/4/2013',
      half_day_on_start_date:   true, 
      end_date:                 '23/4/2013', 
      half_day_on_end_date:     false, 
      date_approved:            '1/1/2013',
      active:                   true,
      approver_id:              md.id)

BookedHoliday.create!(
      user_id:                  gp.id, 
      holiday_year_id:          hy2013.id, 
      start_date:               '10/4/2013',
      half_day_on_start_date:   false, 
      end_date:                 '15/4/2013', 
      half_day_on_end_date:     false, 
      date_approved:            '1/1/2013',
      active:                   false,
      approver_id:              anna.id)

BookedHoliday.create!(
      user_id:                  psr.id, 
      holiday_year_id:          hy2013.id, 
      start_date:               '31/5/2013',
      half_day_on_start_date:   false, 
      end_date:                 '11/6/2013', 
      half_day_on_end_date:     false, 
      date_approved:            '1/1/2013',
      active:                   true,
      approver_id:              md.id)


BookedHoliday.create!(
      user_id:                  na.id, 
      holiday_year_id:          hy2013.id, 
      start_date:               '23/5/2013',
      half_day_on_start_date:   false, 
      end_date:                 '24/6/2013', 
      half_day_on_end_date:     false, 
      date_approved:            '1/1/2013',
      active:                   true,
      approver_id:              md.id)


BookedHoliday.create!(
      user_id:                  na.id, 
      holiday_year_id:          hy2013.id, 
      start_date:               '1/7/2013',
      half_day_on_start_date:   false, 
      end_date:                 '12/7/2013', 
      half_day_on_end_date:     false, 
      date_approved:            '1/1/2013',
      active:                   true,
      approver_id:              md.id)

BookedHoliday.create!(
      user_id:                  na.id, 
      holiday_year_id:          hy2013.id, 
      start_date:               '26/7/2013',
      half_day_on_start_date:   false, 
      end_date:                 '26/7/2013', 
      half_day_on_end_date:     false, 
      date_approved:            '1/1/2013',
      active:                   true,
      approver_id:              md.id)

BookedHoliday.create!(
      user_id:                  na.id, 
      holiday_year_id:          hy2013.id, 
      start_date:               '13/9/2013',
      half_day_on_start_date:   false, 
      end_date:                 '13/9/2013', 
      half_day_on_end_date:     false, 
      date_approved:            '1/1/2013',
      active:                   true,
      approver_id:              md.id)

BookedHoliday.create!(
      user_id:                  na.id, 
      holiday_year_id:          hy2013.id, 
      start_date:               '10/1/2014',
      half_day_on_start_date:   false, 
      end_date:                 '24/1/2014', 
      half_day_on_end_date:     false, 
      date_approved:            '1/1/2013',
      active:                   true,
      approver_id:              md.id)


# and now some for Stepehn's team that have been approved
BookedHoliday.create!(
      user_id:                  wh.id, 
      holiday_year_id:          hy2013.id, 
      start_date:               '23/5/2013',
      half_day_on_start_date:   false, 
      end_date:                 '27/5/2013', 
      half_day_on_end_date:     false, 
      date_approved:            '10/5/2013',
      active:                   true,
      approver_id:              md.id)


BookedHoliday.create!(
      user_id:                  jd.id, 
      holiday_year_id:          hy2013.id, 
      start_date:               '24/5/2013',
      half_day_on_start_date:   false, 
      end_date:                 '29/5/2013', 
      half_day_on_end_date:     false, 
      date_approved:            '10/5/2013',
      active:                   true,
      approver_id:              md.id)


# and some that haven't

BookedHoliday.create!(
      user_id:                  wh.id, 
      holiday_year_id:          hy2013.id, 
      start_date:               '23/7/2013',
      half_day_on_start_date:   false, 
      end_date:                 '27/7/2013', 
      half_day_on_end_date:     false, 
      date_approved:            nil,
      active:                   true,
      approver_id:              nil)


BookedHoliday.create!(
      user_id:                  jd.id, 
      holiday_year_id:          hy2013.id, 
      start_date:               '24/7/2013',
      half_day_on_start_date:   false, 
      end_date:                 '29/7/2013', 
      half_day_on_end_date:     false, 
      date_approved:            nil,
      active:                   true,
      approver_id:              nil)






CompanyDefinedHoliday.create!(
      name:  'Christmas Eve',
      start_date:  Date.new(2013, 12, 24),
      end_date: Date.new(2013,12,24),
      half_day_on_start_date: true,
      half_day_on_end_date: false)



CompanyDefinedHoliday.create!(
      name:  "New Year's Eve",
      start_date:  Date.new(2013, 12, 31),
      end_date: Date.new(2013, 12, 31),
      half_day_on_start_date: true,
      half_day_on_end_date: false)





















