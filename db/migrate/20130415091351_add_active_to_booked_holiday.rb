class AddActiveToBookedHoliday < ActiveRecord::Migration
  def change
    add_column :booked_holidays, :active, :boolean
    execute 'UPDATE booked_holidays set active = 1'
  end
end
