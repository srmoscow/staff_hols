class AddDateRejectedToBookedHoliday < ActiveRecord::Migration
  def change
    add_column :booked_holidays, :date_rejected, :date
  end
end
