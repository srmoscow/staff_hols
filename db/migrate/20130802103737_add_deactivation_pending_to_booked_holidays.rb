class AddDeactivationPendingToBookedHolidays < ActiveRecord::Migration
  def change
    add_column :booked_holidays, :deactivation_pending, :boolean, :default => false
  end
end
