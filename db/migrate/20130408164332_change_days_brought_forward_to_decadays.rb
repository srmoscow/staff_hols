class ChangeDaysBroughtForwardToDecadays < ActiveRecord::Migration
  def up
    rename_column :allowances, :brought_forward, :decadays_brought_forward
  end

  def down
    rename_column :allowances, :decadays_brought_forward, :brought_forward
  end
end
