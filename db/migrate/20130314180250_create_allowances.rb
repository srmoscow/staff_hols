class CreateAllowances < ActiveRecord::Migration
  def change
    create_table :allowances do |t|
      t.integer :user_id
      t.integer :holiday_year_id
      t.integer :num_days
      t.integer :brought_forward
      t.integer :carried_forward
      t.boolean :closed
      t.timestamps
    end
  end
end
