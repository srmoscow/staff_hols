class CreateBookedHolidays < ActiveRecord::Migration
  def change
    create_table :booked_holidays do |t|
      t.integer   :user_id
      t.integer   :holiday_year_id
      t.date      :start_date
      t.boolean   :half_day_on_start_date
      t.date      :end_date
      t.boolean   :half_day_on_end_date
      t.integer   :approver_id
      t.date      :date_approved
      t.integer   :decadays

      t.timestamps
    end
  end
end
