class CreateHolidayYears < ActiveRecord::Migration
  def change
    create_table :holiday_years do |t|
      t.integer :year
      t.string :description
      t.date :start_date
      t.date :end_date

      t.timestamps
    end
  end
end
