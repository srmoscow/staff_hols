class CreateCompanyDefinedHolidays < ActiveRecord::Migration
  def change
    create_table :company_defined_holidays do |t|
      t.string :name
      t.date :start_date
      t.date :end_date
      t.boolean :half_day_on_start_date
      t.boolean :half_day_on_end_date

      t.timestamps
    end
  end
end
