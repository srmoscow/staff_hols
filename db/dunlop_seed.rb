md = User.where(:email => 'steve@tagadab.com').first
jd = User.create( name: 'Jo Dunlop',        email: 'jo@tagadab.com',            is_manager: false, is_admin: false,  manager: md,   username: 'jo.dunlop')

jd.create_allowance(2013, 25)


hy2013 = HolidayYear.where(:year => 2013).first

BookedHoliday.create!(
      user_id:                  jd.id, 
      holiday_year_id:          hy2013.id, 
      start_date:               '20/5/2013',
      half_day_on_start_date:   false, 
      end_date:                 '24/5/2013', 
      half_day_on_end_date:     false, 
      date_approved:            '1/1/2013',
      active:                   true,
      approver_id:              md.id)


