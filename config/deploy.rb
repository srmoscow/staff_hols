# encoding: ASCII-8BIT

# The above line is needed to enable the funny character substitution in the before deploy block below.

$:<< File.join(File.dirname(__FILE__), '..')
require 'bundler/capistrano'
require 'rvm/capistrano'
require 'lib/jabber_message'
require 'nokogiri'
require 'caplock'
# require 'airbrake/capistrano' # This doesnt work (might do when we upgrade to 1.9)
load 'deploy/assets' # Uncomment if we ever move to Rails 3

set :keep_releases, 5 # Deletes old releases up to this limit
set :application, "staff_hols"
set :deploy_to,   "/var/www/#{application}"
set :repository,  "git@git.tagadab.com:apps/staff_hols.git"
set :branch,  'master'

# RVM specific setup
# Use cap deploy:setup to update RVM, install ruby and gemset
set :rvm_ruby_string,   'ruby-1.9.3-p327-falcon@staff_hols'    # use the same ruby as used locally for deployment
set :rvm_autolibs_flag, "read-only"               # more info: rvm help autolibs
set :rvm_type, :system
set :rvm_install_with_sudo, true
before 'deploy:setup',  'rvm:install_rvm'         # install RVM
before 'deploy:setup',  'rvm:install_ruby'        # install Ruby and create gemset, OR:
before 'deploy:setup',  'rvm:create_gemset'       # only create gemset

set :use_sudo, false
set :user, 'deploy'
default_run_options[:pty] = true

app01 = 'staffhols.tagadab.com'

start_time = nil

role :web, app01 

role :app, app01 

gitlab_url = 'http://git.tagadab.com/apps/staff_hols/commits/'

# role :db,  server, primary: true # This is where Rails migrations will run
before :deploy do
  start_time = Time.now
  # Construct Jabber message
  name_of_deployer = `finger \`whoami\` | awk -F: '{ print $3 }' | head -n1 | sed 's/^ //'`.strip

  messages = ["#{name_of_deployer} started a #{application.upcase} deploy (#{Time.now}):"]
  
  existing_revision = capture("if [ -e #{deploy_to}/current/REVISION ]; then cat #{deploy_to}/current/REVISION; else echo 'HEAD'; fi").strip
  if existing_revision != 'HEAD'
    run("cd #{deploy_to}/current && git pull origin #{branch}")
    git_output = capture("cd #{deploy_to}/current && git --no-pager log --pretty=format:\"* %an: %s (#{gitlab_url}%h)\" #{existing_revision}..HEAD").strip
    if git_output.lines.count > 0
      messages += git_output.lines.to_a.map{ |line| line.strip.sub(/^\*/,'■') }
    else
      puts '======= (?) NO CHANGES SINCE LAST DEPLOY (?) ======='
      messages += ["(?) No changes since last deploy (?)"]
    end
  else
    messages << '■ Initial deploy'
  end

  jabber_message(nil, messages.join("\n"))
end

after :deploy do
  name_of_deployer = `finger \`whoami\` | awk -F: '{ print $3 }' | head -n1 | sed 's/^ //'`.strip
  jabber_message(nil,"#{name_of_deployer} finished deploying at #{Time.now} (took #{(Time.now - start_time).round} seconds)")
  begin
    Timeout::timeout(2) do
      execute_remote_command "(echo 'flush_all'; echo 'flush_all'; sleep 2; echo 'quit';) | telnet speeding.tagadab.com 11211"
    end
  rescue => e
    begin
      Timeout::timeout(2) do
        execute_remote_command "(echo 'flush_all'; echo 'flush_all'; sleep 2; echo 'quit';) | telnet bullet.tagadab.com 11211"
      end
    rescue
    end
  end
end

# Passenger restart
namespace :deploy do
  task :start do; end
  task :stop do; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end
end
