# Initialization code for the Holiday Calendar here


class EnHolidayCalendar

  @@calendar = HolidayCalendar.load(:uk_en)

  def self.calendar
    @@calendar
  end
end