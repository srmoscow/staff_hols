

if Rails.env.development?
  if defined?(Footnotes) 
    Footnotes.setup do |config|
      config.before do |controller, filter|
        filter.notes -= [:assigns] if controller.class.name =~ /^Admin::/
        filter.notes -= [:view]
        filter.notes += [:rails_3_view] unless filter.notes.include?(:rails_3_view)
      end
    end

    Footnotes.run! # first of all
  end




# This segment fixes the bug that always show partials count as 0 

  Footnotes::Notes::LogNote::LoggingExtensions.module_eval do
    def add(*args, &block)
      super
      logged_message = args[2] + "\n"
      Footnotes::Notes::LogNote.log(logged_message)
      logged_message
    end
  end

  # This extension requires ApplicationController to expose a method template_filename(action) which returns the name of the view file for the given action
  # This is a new rails_3_view

  module Footnotes
    module Notes
      class Rails3ViewNote < AbstractNote
        def initialize(controller)
          @controller = controller
          @action = @controller.action_name || 'index'
        end

        def row
          :edit
        end

        def to_sym
          :rails_3_view
        end

        def title
          'View'
        end


        def link
          link = escape(Footnotes::Filter.prefix(filename, 1, 1))
          link
        end

        def valid?
          !filename.nil?
        end

        protected

        # def first_render?
        #   @template.instance_variable_get(:@_first_render)
        # end

        def filename
          @controller.template_filename(@action)
        end

      end
    end
  end
end
