
class Date
  def db
    self.strftime('%Y-%m-%d')
  end


  def dmy
    self.strftime('%d/%m/%Y')
  end
  
end


class Time
  def db
    self.strftime('%Y-%m-%d %H:%M:%S')
  end
end