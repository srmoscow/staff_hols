# This is required because the name of the include file isn't the same as the name of the gem.  Fix it, and we won't need this anymore.

unless Rails.env.production?
  require 'cv'
end