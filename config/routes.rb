StaffHols::Application.routes.draw do
  get "admin/index"

  get "calendar/show"

  authenticated :user do
    root :to => 'booked_holidays#index'
  end
  root :to => "booked_holidays#index"
  devise_for :users
  resources :users
  resources :booked_holidays do
    collection do
      post :approve
    end
    member do
      get :approve_deletion
      post :confirm_deletion
    end
  end
end