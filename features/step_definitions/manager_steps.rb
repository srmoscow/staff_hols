

def create_manager
  create_visitor
  delete_manager
  @manager = FactoryGirl.create(:manager, email: @visitor[:email])
  @allowance = FactoryGirl.create :allowance, user_id: @manager.id, holiday_year_id: @holiday_year.id
end

def delete_manager
  @manager ||= User.first conditions: {:email => @visitor[:email]}
  @mangager.destroy unless @manager.nil?
end


def create_holiday_requests(user)
  FactoryGirl.create :unapproved_booked_holiday, user_id: @user.id
end


def create_user_reporting_to(manager)
  @user = FactoryGirl.create :user, manager_id: manager.id
  FactoryGirl.create :allowance, user_id: @user.id
end



Given /^I exist as a manager$/ do
  create_manager
end


Given /^there are outstanding holiday requests$/ do
  create_user_reporting_to(@manager)
  create_holiday_requests(@user)
end


Then /^I should see outstanding holiday requests$/ do
  page.should have_content "Holiday requests awaiting your approval"
  page.should have_button "Approve/Reject Requests"
end