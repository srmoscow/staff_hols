Feature: Manager Sign in
  In order to view staff memebers holiday requests
  A manager
  Should be able to sign in

    Scenario: Manager signs in successfully
      Given I exist as a manager
        And there are outstanding holiday requests
        And I am not logged in
      When I sign in with valid credentials
      Then I see a successful sign in message
      And I should not see an Admin link
      And I should see outstanding holiday requests
      When I return to the site
      Then I should be signed in

    Scenario: Manager enters wrong email
      Given I exist as a manager
      And I am not logged in
      When I sign in with a wrong email
      Then I see an invalid login message
      And I should be signed out
      
    Scenario: Manager enters wrong password
      Given I exist as a manager
      And I am not logged in
      When I sign in with a wrong password
      Then I see an invalid login message
      And I should be signed out

      