require 'spec_helper'

describe AdminController do


  let(:year)          { FactoryGirl.create :holiday_year, year: 2013 }
  let(:admin_user)    { FactoryGirl.create :admin }
  let(:allowances)    { User.all.each { |u| FactoryGirl.create :allowance, user_id: u.id, holiday_year_id: year.id } }


  describe "GET 'index'" do
    it "returns http success for admins" do
      Timecop.freeze(Time.utc(2013,9,1,15,45,32)) do
        admin_user
        allowances
        login_user(admin_user)

        get 'index'
        response.should be_success
      end
    end


    it 'returns http success for managers' do
      Timecop.freeze(Time.utc(2013,9,1,15,45,32)) do
        user = FactoryGirl.create :manager
        allowances
        login_user(user)

        get 'index'
        response.should be_success
      end
    end

    it 'returns 403 forbidden for regular guys' do
      Timecop.freeze(Time.utc(2013,9,1,15,45,32)) do
        user = FactoryGirl.create :user
        allowances
        login_user(user)

        get 'index'
        response.code.should == '403'
      end
    end
  end

end
