require 'spec_helper'

describe BookedHolidaysController do

  context 'actions_for_normal_users' do

    before (:each) do
      @year_1 = FactoryGirl.create :holiday_year, year: 2013
      @user = FactoryGirl.create(:user)
      FactoryGirl.create :allowance, user_id: @user.id, holiday_year_id: @year_1.id
      # sign_in @user
      login_user(@user)
      @bh1 = FactoryGirl.create :booked_holiday, start_date: 2.days.from_now.to_date, end_date: 10.days.from_now.to_date, user: @user
      @bh2 = FactoryGirl.create :booked_holiday, start_date: 22.days.from_now.to_date, end_date: 28.days.from_now.to_date, user: @user
      @bh3 = FactoryGirl.create :booked_holiday, start_date: 32.days.from_now.to_date, end_date: 38.days.from_now.to_date, user: @user
    end


    describe "GET 'index'" do
      it 'should be successful' do
        get :index
        response.should be_success
        assigns[:booked_holidays].size.should == 3
        assigns[:booked_holidays].first.class.should == BookedHolidayDecorator
      end
    end


    describe "POST 'create'" do
      it 'should create call the creator and report when valid' do
        bhc = double(BookedHolidayCreator)
        BookedHolidayCreator.should_receive(:new).and_return(bhc)
        bhc.should_receive(:valid?).and_return(true)
        bhc.should_receive(:notes).and_return('OK')

        post :create, {}
        flash[:notice].should == 'OK'
        response.should be_success
      end

      it 'should create call the creator and report when in error' do
        bhc = double(BookedHolidayCreator)
        BookedHolidayCreator.should_receive(:new).and_return(bhc)
        bhc.should_receive(:valid?).and_return(false)
        bhc.should_receive(:errors).and_return('ERROR')

        post :create, {}
        flash[:errors].should == 'ERROR'
        response.should be_success
      end
    end



    describe "DELETE 'destroy'" do
      it 'should alert and redirect' do
        params = HashWithIndifferentAccess.new( { :id => @bh2.id })
        BookedHoliday.should_receive(:find).with(@bh2.id.to_s).and_return(@bh2)
        @bh2.should_receive(:deactivate!).and_return('my flash message')

        delete :destroy, params
        flash[:alert].should == 'my flash message'
        response.should redirect_to 'http://test.host/'
      end
    end


    describe "POST 'approve'" do
      it 'should run the approver and flash the response' do
        params   = {"controller"=>"booked_holidays", "action"=>"approve"}
        BookedHolidaysController.any_instance.should_receive(:current_user).twice.and_return(@user)

        post :approve, params
        response.code.should == '403'
      end

      it 'should not run the approver if invalid' do
        params   = {"controller"=>"booked_holidays", "action"=>"approve"}
        BookedHolidaysController.any_instance.should_receive(:current_user).twice.and_return(@user)

        post :approve, params
        response.code.should == '403'
      end
    end



    describe "GET 'approve_deletion'" do
      it 'should return forbidden 403 for regular user' do
        bh   = double BookedHoliday
        bhd  = double BookedHolidayDecorator
        
        params = HashWithIndifferentAccess.new( {:id => @user.id } )

        get :approve_deletion, params

        response.code.should == '403'
      end
    end


    describe "POST 'confirm_deletion'" do
      it 'should return forbidden 403 for regular user' do
        params = HashWithIndifferentAccess.new({"id"=>"99", "controller"=>"booked_holidays", "action"=>"confirm_deletion"})
        bhdp = double BookedHolidayDeletionProcessor

        post :confirm_deletion, params

        response.code.should == '403'
      end
    end
  end


  context 'actions requiring manager role' do
    before (:each) do
      @user = FactoryGirl.create(:user)
      @manager = FactoryGirl.create(:manager)
      login_user(@manager)
      @bh1 = FactoryGirl.create :booked_holiday, start_date: 2.days.from_now.to_date, end_date: 10.days.from_now.to_date, user: @user
      @bh2 = FactoryGirl.create :booked_holiday, start_date: 22.days.from_now.to_date, end_date: 28.days.from_now.to_date, user: @user
      @bh3 = FactoryGirl.create :booked_holiday, start_date: 32.days.from_now.to_date, end_date: 38.days.from_now.to_date, user: @user
    end

    describe "POST 'confirm_deletion'" do
      it 'should call the BookedHolidayDeletionProcessor and redirect' do
        params = HashWithIndifferentAccess.new({"id"=>"99", "controller"=>"booked_holidays", "action"=>"confirm_deletion"})
        bhdp = double BookedHolidayDeletionProcessor
        BookedHolidayDeletionProcessor.should_receive(:new).with(params).and_return(bhdp)
        bhdp.should_receive(:run).and_return('Output from BookedHolidayDeletionProcessor')

        post :confirm_deletion, params

        response.should redirect_to(booked_holidays_path)
        flash[:notice].should == 'Output from BookedHolidayDeletionProcessor'
      end
    end


    describe "GET 'approve_deletion'" do
      it 'should load the booked holiday and user and return success' do
        bh   = double BookedHoliday
        bhd  = double BookedHolidayDecorator
        BookedHoliday.should_receive(:find).with(@user.id.to_s).and_return(bh)
        bh.should_receive(:decorate).and_return(bhd)
        bhd.should_receive(:user).and_return(@user)

        params = HashWithIndifferentAccess.new( {:id => @user.id.to_s} )

        get :approve_deletion, params

        response.should be_success
        assigns[:booked_holiday].should == bhd
        assigns[:user].should == @user
      end
    end



    describe "POST 'approve'" do
      it 'should run the approver and flash the response' do
        params   = {"controller"=>"booked_holidays", "action"=>"approve"}
        approver = double(BookedHolidayApprover)
        BookedHolidaysController.any_instance.should_receive(:current_user).twice.and_return(@manager)
        BookedHolidayApprover.should_receive(:new).with(@manager, params).and_return(approver)
        approver.should_receive(:valid?).and_return(true)
        approver.should_receive(:run)
        approver.should_receive(:notes).and_return("xxxxx")

        post :approve, params
        response.should redirect_to 'http://test.host/'
        flash[:alert].should == "xxxxx"
      end

      it 'should not run the approver if invalid' do
        params   = {"controller"=>"booked_holidays", "action"=>"approve"}
        approver = double(BookedHolidayApprover)
        BookedHolidaysController.any_instance.should_receive(:current_user).twice.and_return(@manager)
        BookedHolidayApprover.should_receive(:new).with(@manager, params).and_return(approver)
        approver.should_receive(:valid?).and_return(false)
        approver.should_receive(:run).never
        approver.should_receive(:notes).and_return("xxxxx")

        post :approve, params
        response.should redirect_to 'http://test.host/'
        flash[:alert].should == "xxxxx"
      end
    end



  end

end



















