require 'spec_helper'

describe UsersController do

  before (:each) do
    @admin = FactoryGirl.create(:admin)
    @user_1 = FactoryGirl.create :user
    @user_2 = FactoryGirl.create :user
    sign_in @admin
  end


  describe "GET 'index'" do
    it 'should be successful' do
      get :index
      response.should be_success
      assigns[:users].should == [ @admin.manager, @admin, @user_1.manager, @user_1, @user_2.manager, @user_2 ]
    end
  end



  describe "GET 'show'" do
    
    it "should be successful" do
      get :show, :id => @user_1.id
      response.should be_success
    end
    
    it "should find the right user" do
      get :show, :id => @user_1.id
      assigns(:user).should == @user_1
    end
    
  end



  describe "POST 'update'" do
    it 'should update the user' do
      params = HashWithIndifferentAccess.new({"user"=>{"is_admin"=>"0", "is_manager"=>"1"}, "commit"=>"Change Role", "id"=>@user_1.id})

      post :update, params

      flash[:notice].should ==  "User updated."
      response.should redirect_to users_path
      @user_1.reload.is_manager?.should be_true
    end


    it 'should flash if unable to update' do
      params = HashWithIndifferentAccess.new({"user"=>{"is_admin"=>"0", "is_manager"=>"1"}, "commit"=>"Change Role", "id"=>@user_1.id.to_s})
      User.should_receive(:find).with(@user_1.id.to_s).twice.and_return(@user_1)
      @user_1.should_receive(:update_roles).with(params[:user]).and_return(false)

      post :update, params

      flash[:alert].should ==  "Unable to update user."
      response.should redirect_to users_path
    end

  end


  describe "DELETE 'destroy'" do
    it 'should destroy the named user' do
      params = HashWithIndifferentAccess.new( { :id => @user_1.id.to_s } )
      User.should_receive(:find).exactly(2).times.with(params[:id]).and_return(@user_1)
      @user_1.should_receive(:destroy).and_return(@user_1.freeze)

      delete :destroy, params
      flash[:notice].should == "User deleted."
    end


    it 'should error if you try to delete yourself' do
      params = HashWithIndifferentAccess.new( { :id => @admin.id.to_s } )

      delete :destroy, params
      flash[:notice].should == "Can't delete yourself."
    end


  end

end





