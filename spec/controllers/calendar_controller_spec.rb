require 'spec_helper'

describe CalendarController do


  before (:each) do
    @user = FactoryGirl.create(:user)
    sign_in @user
  end

  describe "GET 'show'" do
    it 'should be successful if given an id' do
      params     = HashWithIndifferentAccess.new( { :id => 133 })
      bh         = double BookedHoliday
      bhd        = double BookedHolidayDecorator
      start_date = double Date
      end_date   = double Date
      calendar   = double StaffHolidayCalendar
      monthstart = double Date

      BookedHoliday.should_receive(:find).with(params[:id].to_s).and_return(bh)
      bh.should_receive(:decorate).and_return(bhd)
      bhd.should_receive(:start_date_for_calendar_display).and_return(start_date)
      bhd.should_receive(:end_date_for_calendar_display).and_return(end_date)
      bhd.should_receive(:start_date).and_return(start_date)
      start_date.should_receive(:beginning_of_month).and_return(monthstart)
      StaffHolidayCalendar.should_receive(:new).with(start_date, end_date).and_return(calendar)

      get :show, params

      response.should be_success
      assigns[:calendar].should == calendar
      assigns[:type].should == :booked_holiday
      assigns[:date].should == monthstart
    end




    it 'should be successful  if given a date' do
      params     = HashWithIndifferentAccess.new( { :date => '2013-08-13' })
      get :show, params
      response.should be_success
      assigns[:calendar].class.should == StaffHolidayCalendar
      assigns[:type].should == :month
      assigns[:date].should == Date.new(2013, 8, 1)
    end


    it 'should raise an error if there is no date or id param' do
      params = HashWithIndifferentAccess.new
      expect {
        get :show, params
      }.to raise_error RuntimeError, "No id or date parameter"
    end
  end
end




