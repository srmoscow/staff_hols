require 'simplecov'
require 'simplecov-rcov'

SimpleCov.formatter = SimpleCov::Formatter::MultiFormatter[
  SimpleCov::Formatter::HTMLFormatter,
  SimpleCov::Formatter::RcovFormatter
]

SimpleCov.start do
  # quickly redefine the 'rails' adapter to remove plugins group
  add_filter '/spec/'
  add_filter '/config/'
  add_filter '/app/admin'

  add_group 'Controllers', File.expand_path(File.dirname(__FILE__) + '../../app/controllers')
  add_group 'Models'     , File.expand_path(File.dirname(__FILE__) + '../../app/models')
  add_group 'Helpers'    , File.expand_path(File.dirname(__FILE__) + '../../app/helpers')
  add_group 'Services'   , File.expand_path(File.dirname(__FILE__) + '../../app/services')
  add_group 'Decorators' , File.expand_path(File.dirname(__FILE__) + '../../app/decorators')
  add_group 'Validators' , File.expand_path(File.dirname(__FILE__) + '../../app/validators')
  add_group 'Mailers'    , File.expand_path(File.dirname(__FILE__) + '../../app/mailers')
  add_group 'Libraries'  , File.expand_path(File.dirname(__FILE__) + '../../lib')
  add_group 'ActiveAdmin', File.expand_path(File.dirname(__FILE__) + '../../app/admin')
end