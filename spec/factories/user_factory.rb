# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  name                   :string(255)
#  is_admin               :boolean
#  manager_id             :integer
#  is_manager             :boolean
#  username               :string(255)
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  
  sequence(:name)             { |n| "User No. #{n}"}
  sequence(:username)         { |n| "user_#{n}"}
  sequence(:email)            { |n| "user_#{n}@example.com"}

  factory :user do
    association               :manager, factory: :manager
    name
    username
    email
    is_admin                  false
    is_manager                false
    

    factory :admin do
      is_admin                true
    end

    factory :manager do
      manager                 nil
      is_manager              true
    end

    factory :admin_manager do
      is_admin                true
      is_manager              true
    end
  end
end
