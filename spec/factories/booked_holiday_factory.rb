# == Schema Information
#
# Table name: booked_holidays
#
#  id                     :integer          not null, primary key
#  user_id                :integer
#  holiday_year_id        :integer
#  start_date             :date
#  half_day_on_start_date :boolean
#  end_date               :date
#  half_day_on_end_date   :boolean
#  approver_id            :integer
#  date_approved          :date
#  decadays               :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  date_rejected          :date
#  active                 :boolean
#  deactivation_pending   :boolean          default(FALSE)
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :booked_holiday do
    association                 :user, factory: :user
    start_date                  Date.new(2013, 3, 7)
    half_day_on_start_date      false
    end_date                    Date.new(2013, 3, 13)
    half_day_on_end_date        false
    approver_id                 { user.manager.id }
    date_approved               Date.today
    active                      true
    holiday_year                { HolidayYear.find_or_create_for_date(start_date) }

    factory :five_day_booked_holiday_in_march_2014 do
      association               :holiday_year, factory: :holiday_year, :year => 2014
      start_date                Date.new(2014, 3, 3)
      end_date                  Date.new(2014, 3, 7)
    end

    factory :future_booked_holiday do
      association               :holiday_year, factory: :holiday_year, :year => 2014
      start_date                1.day.from_now.to_date
      end_date                  5.days.from_now.to_date
    
      factory :past_booked_holiday do
        start_date                10.days.ago.to_date
        end_date                  5.days.ago.to_date
      end

      factory :current_booked_holiday do
        start_date                2.days.ago.to_date
        end_date                  5.days.from_now.to_date
      end

    

      factory :four_and_a_half_day_booked_holiday_in_june_2014 do
        start_date                Date.new(2014, 6, 2)
        end_date                  Date.new(2014, 6, 6)
        half_day_on_start_date    true
      end
    end

    factory :rejected_booked_holiday do
      date_rejected             '13/3/2013'
      date_approved             nil
    end


    factory :unapproved_booked_holiday do
      approver_id              nil
      date_approved            nil

      factory :three_day_unapproved_holiday_in_july_2014 do
        start_date              Date.new(2014, 7, 4)
        end_date                Date.new(2014, 7, 8)
      end

      factory :half_day_unapproved_booked_holiday_in_july_2014 do
        start_date              Date.new(2014, 7, 22)
        end_date                Date.new(2014, 7, 22)
        half_day_on_start_date  true
      end
    end
  end
end
