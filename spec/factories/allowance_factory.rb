# == Schema Information
#
# Table name: allowances
#
#  id                       :integer          not null, primary key
#  user_id                  :integer
#  holiday_year_id          :integer
#  num_days                 :integer
#  decadays_brought_forward :integer
#  carried_forward          :integer
#  closed                   :boolean
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :allowance do
    user_id                   1
    holiday_year_id           9
    num_days                  20
    decadays_brought_forward  50
    carried_forward           0
  end
end
