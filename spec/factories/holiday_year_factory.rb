# == Schema Information
#
# Table name: holiday_years
#
#  id          :integer          not null, primary key
#  year        :integer
#  description :string(255)
#  start_date  :date
#  end_date    :date
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :holiday_year do
    year                  2012
    description           { |hy| "Year beginning April #{hy.year}" }
    start_date            { |hy| Date.new(hy.year, 4, 1) }
    end_date              { |hy| Date.new(hy.year + 1, 3, 31) }
  


    factory :current_holiday_year  do
      year                  { get_current_year_number }
    end


    factory :last_holiday_year do
      year                  { get_current_year_number - 1 }
    end

    factory :next_holiday_year do
      year                  { get_current_year_number + 1 }
    end
  end
end



def get_current_year_number
  curdate = Date.today
  curdate.month < 4 ? curdate.year - 1 : curdate.year
end
