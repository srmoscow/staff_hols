# == Schema Information
#
# Table name: company_defined_holidays
#
#  id                     :integer          not null, primary key
#  name                   :string(255)
#  start_date             :date
#  end_date               :date
#  half_day_on_start_date :boolean
#  half_day_on_end_date   :boolean
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :cdh_xmas_ny, class: CompanyDefinedHoliday do
    name                      "between Christmas and New Year"
    start_date                "2013-12-30"
    end_date                  "2013-12-31"
    half_day_on_start_date    false
    half_day_on_end_date      false

    factory :cdh_xmas_eve_half_day do
      name                      "Christmas Eve"
      start_date                "2013-12-24"
      end_date                  "2013-12-24"
      half_day_on_start_date    true
      half_day_on_end_date      false
    end

    factory :new_years_eve_half_day do
      name                      "New Years Eve"
      start_date                "2013-12-31"
      end_date                  "2013-12-31"
      half_day_on_start_date    true
      half_day_on_end_date      false
    end


    factory :cdh_spans_weekend do
      name                      "Invalid on Sunday"
      start_date                "2013-05-16"
      end_date                  "2013-05-20"
      half_day_on_start_date    true
      half_day_on_end_date      false
    end

    factory :cdh_one_day_two_half_days do
      name                      "Start and End both half days on same day"
      start_date                "2013-05-16"
      end_date                  "2013-05-16"
      half_day_on_start_date    true
      half_day_on_end_date      true
    end

    factory :cdh_overlap do
      name                      "Start and End both half days on same day"
      start_date                "2013-12-31"
      end_date                  "2013-12-31"
      half_day_on_start_date    false
      half_day_on_end_date      false
    end

  end    
end
