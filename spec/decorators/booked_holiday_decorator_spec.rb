require 'spec_helper'

describe BookedHolidayDecorator do


  describe '#to_s' do
    it 'should provide a human-readable description of the holiday dates' do
      bh = FactoryGirl.create :booked_holiday
      bh.decorate.to_s.should == 'From 07/03/2013 to 13/03/2013 (5.0 working days)'
    end
  end



  describe '#approved_by' do
    it 'should return the name of the approver if approved' do
      bh      = FactoryGirl.build :booked_holiday
      bh.decorate.approval_status.should == bh.user.manager.name
    end

    it 'should return awaiting approval if unapproved' do
      bh      = FactoryGirl.build :unapproved_booked_holiday
      bh.decorate.approval_status.should == 'Awaiting Approval'
    end

    it 'should return rejected if rejected' do
      bh = FactoryGirl.build :rejected_booked_holiday
      bh.decorate.approval_status.should == 'Rejected'
    end

  end


  describe 'date_formats' do
    let(:bh) {
      bh = FactoryGirl.build :booked_holiday
      bh.decorate
    }

    it { bh.start_date_db.should    == '2013-03-07' }
    it { bh.end_date_db.should      == '2013-03-13' }
    it { bh.start_date_human.should == 'Thu 07 Mar 2013'}
    it { bh.end_date_human.should   == 'Wed 13 Mar 2013'}
    
  end



  describe '#deletion_confirmation_message' do
    it 'should display the future message for holidays in the future' do
      bhd = FactoryGirl.build(:future_booked_holiday).decorate
      bhd.deletion_confirmation_message.should == "Are you sure you want to delete the future booked holiday #{bhd.to_s}?"
    end

    it 'should display past deletion confirmation message for holidays int he past' do
      bhd = FactoryGirl.build(:past_booked_holiday).decorate
      bhd.deletion_confirmation_message.should == "The booked holiday #{bhd.to_s} is in the past, and you cannot delete it. However, if you press OK, a message will be sent to your manager asking him to delete it.  Go ahead?"
    end

    it 'should display past deletion confirmation message for holidays which have started but not yet finished' do
      bhd = FactoryGirl.build(:current_booked_holiday).decorate
      bhd.deletion_confirmation_message.should == "The booked holiday #{bhd.to_s} is in the past, and you cannot delete it. However, if you press OK, a message will be sent to your manager asking him to delete it.  Go ahead?"
    end


  end




end
