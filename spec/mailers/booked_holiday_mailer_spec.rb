require "spec_helper"

describe BookedHolidayMailer do

  let(:booked_holiday)                      { 
    bh = FactoryGirl.create :booked_holiday 
    allowance = FactoryGirl.create :allowance, :user_id => bh.user_id, :holiday_year_id => bh.holiday_year_id
    bh
  }
  let(:rejected_booked_holiday)             {FactoryGirl.create :rejected_booked_holiday}

  let(:holiday_request_mail)                { BookedHolidayMailer.holiday_request_mail(booked_holiday) }
  let(:approval_mail)                       { BookedHolidayMailer.approve_holiday_request_mail(booked_holiday) }

  let(:past_booked_holiday_deleted_mail)    { BookedHolidayMailer.past_booked_holiday_deleted_mail(booked_holiday) }
  let(:future_booked_holiday_deleted_mail)  { BookedHolidayMailer.future_booked_holiday_deleted_mail(booked_holiday) }
  let(:deletion_approval_mail)              { BookedHolidayMailer.approve_deletion_request(booked_holiday, "I have approved this") }
  let(:deletion_denial_mail)                { BookedHolidayMailer.deny_deletion_request(booked_holiday, "I will not approve this") }
  


  describe 'holiday_request_mail' do
    it 'renders the subject' do
      holiday_request_mail.subject.should == "**** TEST **** Holiday Request from #{booked_holiday.user.name}"
    end

    it 'renders the addressee' do
      holiday_request_mail.to.should == [ booked_holiday.user.manager.email ]
    end

    it 'renders the sender' do
      holiday_request_mail.from.should == [ 'test_staff_hols@tagadab.com' ]
    end

    it 'renders the body' do
      holiday_request_mail.body.encoded.should  match(/User No\. \d+ has submitted a holiday request From 07\/03\/2013 to 13\/03\/2013 \(5\.0 working days\)\./)
    end
  end



  describe 'past_booked_holiday_deleted_mail' do
    it 'renders the subject' do
      past_booked_holiday_deleted_mail.subject.should == "**** TEST ****  Deletion of Booked Holiday in the past requested"
    end

    it 'renders the addressee' do
      past_booked_holiday_deleted_mail.to.should == [ booked_holiday.user.manager.email ]
    end

    it 'renders the sender' do
      past_booked_holiday_deleted_mail.from.should == [ 'test_staff_hols@tagadab.com' ]
    end

    it 'renders the body' do
      past_booked_holiday_deleted_mail.body.encoded.should  match(/Message from Staff Holiday Booking System:  Deletion Request for past Booked Holiday/)
    end
  end




  describe 'future_booked_holiday_deleted_mail' do
    it 'renders the subject' do
      future_booked_holiday_deleted_mail.subject.should == "**** TEST ****  Deletion of future Booked Holiday"
    end

    it 'renders the addressee' do
      future_booked_holiday_deleted_mail.to.should == [ booked_holiday.user.manager.email ]
    end

    it 'renders the sender' do
      future_booked_holiday_deleted_mail.from.should == [ 'test_staff_hols@tagadab.com' ]
    end

    it 'renders the body' do
      future_booked_holiday_deleted_mail.body.encoded.should  match(/Message from Staff Holiday Booking System:  Deletion of future Booked Holiday for /)
    end
  end



  describe 'approve_deletion_request' do
    it 'renders the subject' do
      deletion_approval_mail.subject.should == "**** TEST ****  Booked Holiday Deletion Request Approved"
    end

    it 'renders the addressee' do
      deletion_approval_mail.to.should == [ booked_holiday.user.email ]
    end

    it 'renders the body' do
      deletion_approval_mail.body.encoded.should  match(/Message from Staff Holiday Booking System:  Deletion Request for holiday Approved/)
      deletion_approval_mail.body.encoded.should  match(/The following message was attached to the approval:/)
      deletion_approval_mail.body.encoded.should  match(/I have approved this/)
    end
  end



 
  describe 'deny_deletion_request' do
    it 'renders the subject' do
      deletion_denial_mail.subject.should == "**** TEST ****  Booked Holiday Deletion Request Denied"
    end

    it 'renders the addressee' do
      deletion_denial_mail.to.should == [ booked_holiday.user.email ]
    end

    it 'renders the body' do
      deletion_denial_mail.body.encoded.should  match(/Message from Staff Holiday Booking System:  Deletion Request for holiday Denied/)
      deletion_denial_mail.body.encoded.should  match(/The following message was attached to the denial:/)
      deletion_denial_mail.body.encoded.should  match(/I will not approve this/)
    end
  end


end




