require 'spec_helper'

describe BookedHolidayDeletionProcessor do


  describe '#run' do
    it 'should mark the record as deleted and send an approved mail with a message' do
      bh     = FactoryGirl.create :booked_holiday, :active => true, :deactivation_pending => true
      params = HashWithIndifferentAccess.new( {"email_message"=>"This is the message I want to give.", "approve_deletion"=>"", "action"=>"confirm_deletion", "controller"=>"booked_holidays", "id"=>"#{bh.id}"} )
      mailer = double BookedHolidayMailer
     
      BookedHolidayMailer.should_receive(:approve_deletion_request).with(bh, "This is the message I want to give.").and_return(mailer)
      mailer.should_receive(:deliver)

      BookedHolidayDeletionProcessor.new(params).run
      bh.reload
      bh.active.should be_false
      bh.deactivation_pending.should be_false
    end


    it 'should mark the record sa deleted and send an approved mail with no message' do
      bh     = FactoryGirl.create :booked_holiday, :active => true, :deactivation_pending => true
      params = HashWithIndifferentAccess.new( {"email_message"=>"", "approve_deletion"=>"", "action"=>"confirm_deletion", "controller"=>"booked_holidays", "id"=>"#{bh.id}"} )
      mailer = double BookedHolidayMailer
     
      BookedHolidayMailer.should_receive(:approve_deletion_request).with(bh, nil).and_return(mailer)
      mailer.should_receive(:deliver)

      BookedHolidayDeletionProcessor.new(params).run
      bh.reload
      bh.active.should be_false
      bh.deactivation_pending.should be_false
    end


    it 'should mark the record as active and no deactivation pending with a message' do
      bh     = FactoryGirl.create :booked_holiday, :active => true, :deactivation_pending => true
      params = HashWithIndifferentAccess.new( {"email_message"=>"There's no way you're deleting a holiday you've already taken!", "deny_deletion"=>"", "action"=>"confirm_deletion", "controller"=>"booked_holidays", "id"=>"#{bh.id}"} )
      mailer = double BookedHolidayMailer
     
      BookedHolidayMailer.should_receive(:deny_deletion_request).with(bh, "There's no way you're deleting a holiday you've already taken!").and_return(mailer)
      mailer.should_receive(:deliver)

      BookedHolidayDeletionProcessor.new(params).run
      bh.reload
      bh.active.should be_true
      bh.deactivation_pending.should be_false
    end


    it 'should mark the record as active and no deactivation pending with no message' do
      bh     = FactoryGirl.create :booked_holiday, :active => true, :deactivation_pending => true
      params = HashWithIndifferentAccess.new( {"email_message"=>"", "deny_deletion"=>"", "action"=>"confirm_deletion", "controller"=>"booked_holidays", "id"=>"#{bh.id}"} )
      mailer = double BookedHolidayMailer
     
      BookedHolidayMailer.should_receive(:deny_deletion_request).with(bh, nil).and_return(mailer)
      mailer.should_receive(:deliver)

      BookedHolidayDeletionProcessor.new(params).run
      bh.reload
      bh.active.should be_true
      bh.deactivation_pending.should be_false
    end


    it 'should raise if no param to indicate what button was pressed ' do
      bh     = FactoryGirl.create :booked_holiday, :active => true, :deactivation_pending => true
      params = HashWithIndifferentAccess.new( {"email_message"=>"", "action"=>"confirm_deletion", "controller"=>"booked_holidays", "id"=>"#{bh.id}"} )
      expect {
        BookedHolidayDeletionProcessor.new(params).run
      }.to raise_error RuntimeError, %Q[No approve_deletion or deny_deletion key in the params hash: {"email_message"=>"", "action"=>"confirm_deletion", "controller"=>"booked_holidays", "id"=>"#{bh.id}"}]
    end


  end

end