require 'spec_helper'

describe BookedHolidayApprover do


  let(:params) {
     ActiveSupport::HashWithIndifferentAccess.new(
        { "utf8"               => "x",
          "authenticity_token" => "elqKdYu9CdsPKqcIbgdZd5ImK84wyxrHdXI8NG9x5Do=",
          "approve_36"         => "1",
          "reject_38"          => "1",
          "action"             => "approve",
          "controller"         => "booked_holidays"})
    }

    let(:manager) { FactoryGirl.create(:manager) }


  describe '.new' do
    it 'should not be valid if user is not manager or admin' do
      user = FactoryGirl.create :user
      bha = BookedHolidayApprover.new(user, params)
      bha.valid?.should be_false
      bha.errors.should == ["User #{user.name} does not have rights to approve or reject holiday requests"]
    end


    it 'should not be valid if no approve or rejects where specified' do
      params.delete(:approve_36)
      params.delete(:reject_38)
      bha = BookedHolidayApprover.new(manager, params)
      bha.valid?.should be_false
      bha.errors.should == ['No Booked Holidays were approved or rejected']
    end

    it 'should be valid if user is manager and there is an approval' do
      params.delete(:reject_38)
      bha = BookedHolidayApprover.new(manager, params)
      bha.valid?.should be_true
    end

  end

  describe '#errors_for_flash' do
    it 'should format the errors suitable for displaying in flash' do
      bha = BookedHolidayApprover.new(manager, params)
      bha.instance_variable_set(:@errors, ['Error 1', 'Error 2'])
      bha.errors.should == ['Error 1', 'Error 2']
    end
  end


  describe '#run' do

    it 'should find the booked holiday and reject or approve it' do
      bh_approved = FactoryGirl.create :booked_holiday
      bh_rejected = FactoryGirl.create :rejected_booked_holiday
      FactoryGirl.create :allowance, user_id: bh_approved.user_id, holiday_year_id: bh_approved.holiday_year_id
      FactoryGirl.create :allowance, user_id: bh_rejected.user_id, holiday_year_id: bh_rejected.holiday_year_id
      params.delete('approve_36')
      params.delete('reject_38')
      params["approve_#{bh_approved.id}"] = '1'
      params["reject_#{bh_rejected.id}"] = '1'
      bha = BookedHolidayApprover.new(manager, params)
      bha.run
      bha.notes.should ==  [
            "Booked Holiday From 07/03/2013 to 13/03/2013 (5.0 working days) for #{bh_approved.user.name} has been approved and a mail has been sent.",
            "Booked Holiday From 07/03/2013 to 13/03/2013 (5.0 working days) for #{bh_rejected.user.name} has been rejected and a mail has been sent."
        ]
    end
  end
end





