require 'spec_helper'

describe BookedHolidayCreator do
  
  let(:params) {
      ActiveSupport::HashWithIndifferentAccess.new(
        { "start_date"             =>"25/3/2013", 
          "half_day_on_start_date" =>"1", 
          "end_date"               =>"5/4/2013", 
          "user_id"                =>"22"
        } )
    }

  before(:each) do
    BookedHolidayMailer.deliveries = []
    @manager = FactoryGirl.create :manager
    @user = FactoryGirl.create :user, id: 22, manager_id: @manager.id
    @y2012 = FactoryGirl.create :holiday_year, year: 2012
    @y2013 = FactoryGirl.create :holiday_year, year: 2013
    FactoryGirl.create :allowance, user_id: @user.id, holiday_year_id: @y2012.id
    FactoryGirl.create :allowance, user_id: @user.id, holiday_year_id: @y2013.id
  end


  describe 'initialize' do
    it 'should create one booked holiday for a period that falls in one year' do
      params[:start_date] = '20/3/2013'
      params[:end_date] = '28/3/2013'
      bhc = BookedHolidayCreator.new(params)
      bhc.should be_valid
      bhc.errors.should be_empty
      bhc.booked_holidays.size.should == 1
      bhc.booked_holidays.first.decorate.to_s.should == 'From 20/03/2013 to 28/03/2013 (6.5 working days)'
    end

    it 'should create one mail to the manager' do
      params[:start_date] = '20/3/2013'
      params[:end_date] = '28/3/2013'
      bhc = BookedHolidayCreator.new(params)

      BookedHolidayMailer.deliveries.size.should == 1
      mail = BookedHolidayMailer.deliveries.first
      mail[:from].to_s.should == 'TestHols <test_staff_hols@tagadab.com>'
      mail.subject.should == "**** TEST **** Holiday Request from #{@user.name}"
    end

    it 'should create two booked holidays for a period that crosses end-of-year' do
      # BookedHolidayMailer.should_receive(:holiday_request_mail).twice
      bhc = BookedHolidayCreator.new(params)
      bhc.should be_valid
      bhc.errors.should be_empty
      bhc.booked_holidays.size.should == 2
      bhc.booked_holidays.first.decorate.to_s.should == 'From 25/03/2013 to 31/03/2013 (3.5 working days)'
      bhc.booked_holidays.last.decorate.to_s.should == 'From 01/04/2013 to 05/04/2013 (4.0 working days)'
    end

    it 'should create two emails for a period that crosses the end of year' do
      bhc = BookedHolidayCreator.new(params)
      BookedHolidayMailer.deliveries.size.should == 2
    end

    it 'should populate the errors array if there is an error' do
      params[:start_date] = '20/3/2013'
      params[:end_date] = '28/3/2013'
      bhc1 = BookedHolidayCreator.new(params)
      bhc1.errors.should be_empty

      bhc2 = BookedHolidayCreator.new(params)
      bhc2.errors.should_not be_empty
      bhc2.errors.should == ['ActiveRecord::RecordInvalid Validation failed: This holiday overlaps with an existing holiday']
    end

  end


  


  describe 'private method split_dates' do

    

    it 'should return two arrays of details' do
      
      bhc = BookedHolidayCreator.new( params )
      start_date = Date.new(2013,3,25)
      end_date = Date.new(2013,4,5)
      result = bhc.send(:split_dates)
      result.should be_instance_of(Array)
      result.size.should == 2
      result.first.should == [@y2012.id, start_date, true, Date.new(2013,3,31), false, 22]
      result.last.should == [@y2013.id, Date.new(2013,4,1), false, end_date, false, 22]
    end
  end

end
