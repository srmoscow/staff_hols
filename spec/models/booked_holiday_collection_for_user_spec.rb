
require 'spec_helper'

describe BookedHolidayCollectionForUser do

  describe '.new' do
    it 'should store intantiate an object as expected' do
      mock_user  = double(User)
      User.should_receive(:find).with(25).and_return(mock_user)

      bhcfu = BookedHolidayCollectionForUser.new(25, ['a', 'b', 'c'])
      bhcfu.user_id.should == 25
      bhcfu.user.should == mock_user
      bhcfu.booked_holidays.should == ['a', 'b', 'c']
    end
  end



  describe '.group_by_user' do
    it 'should return an array of BookedHolidayCollectionForUser objects - one for each user' do
      bhu1h1 = FactoryGirl.create :booked_holiday, start_date: 4.days.ago.to_date, end_date: 2.days.from_now.to_date
      bhu2h1 = FactoryGirl.create :booked_holiday, start_date: 5.days.ago.to_date, end_date: 2.days.from_now.to_date
      bhu1h2 = FactoryGirl.create :booked_holiday, start_date: 8.days.from_now.to_date, end_date: 12.days.from_now.to_date, user: bhu1h1.user


      collections = BookedHolidayCollectionForUser.group_by_user( [ bhu1h1, bhu2h1, bhu1h2 ])
      collections.size.should == 2
      coll1 = collections.first
      coll1.size.should == 2
      coll1.user_id.should == bhu1h1.user_id
      coll1.booked_holidays.should == [ bhu1h1, bhu1h2 ]

      coll2 = collections.last
      coll2.size.should == 1
      coll2.user_id.should == bhu2h1.user_id
      coll2.booked_holidays.should == [ bhu2h1 ]



    end
  end

end

  