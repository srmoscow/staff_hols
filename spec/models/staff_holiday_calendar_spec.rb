require 'spec_helper'

describe StaffHolidayCalendar do

  describe '.new' do

    let(:start_date)      { 5.days.ago.to_date }
    let(:end_date)        { 1.day.ago.to_date  }

    it 'should raise if end date not after start date' do
      expect { StaffHolidayCalendar.new(Date.today, 2.days.ago) }.to raise_error ArgumentError, 'End date must be after start date'
    end

    it 'should raise if start date and end date are the same' do
      expect { StaffHolidayCalendar.new(Date.today, Date.today) }.to raise_error ArgumentError, 'End date must be after start date'
    end

    it 'should call complete initialization if dates are valid' do
      booked_hols        = double('Array of Booked Holiday records')
      sorted_booked_hols = double('Sorted Array of Booked Holiday Records')

      BookedHoliday.should_receive(:all_for_period).with(start_date, end_date).and_return(booked_hols)
      booked_hols.should_receive(:sort).and_return(sorted_booked_hols)
      BookedHolidayCollectionForUser.should_receive(:group_by_user).with(sorted_booked_hols)
      StaffHolidayCalendar.any_instance.should_receive(:generate_days).and_return([])

      StaffHolidayCalendar.new(start_date, end_date)
    end
  end


  describe('.new_for_month') do
    let(:first_day)     { Date.new(2013, 8, 1)  }
    let(:last_day)      { Date.new(2013, 8, 31) }
    let(:middle_day)    { Date.new(2013, 8, 13) }

    it 'should call with the first and last dates of the month' do
      StaffHolidayCalendar.should_receive(:new).with(first_day, last_day)
      StaffHolidayCalendar.new_for_month(middle_day)
    end
  end



  describe 'date formatting methods' do
    let(:first_day)     { Date.new(2013, 8, 1)  }
    let(:last_day)      { Date.new(2013, 8, 31) }
    let(:cal)           { StaffHolidayCalendar.new(first_day, last_day) }

    describe '#start_date_human' do
      it { cal.start_date_human.should == 'Thu 01 Aug 2013' }
    end

    describe '#start_date_month_name' do
      it { cal.start_date_month_name.should == 'August 2013' }
    end
  end




  describe 'private method #generate days' do
    it 'should call StaffHolidayDay.new once for every day in the period' do
      calendar = StaffHolidayCalendar.new(Date.new(2013, 8, 1), Date.new(2013, 8, 4))
      shd1 = double(StaffHolidayDay)
      shd2 = double(StaffHolidayDay)
      shd3 = double(StaffHolidayDay)
      shd4 = double(StaffHolidayDay)

      StaffHolidayDay.should_receive(:new).with(Date.new(2013, 8, 1), instance_of(Array) ).and_return(shd1)
      StaffHolidayDay.should_receive(:new).with(Date.new(2013, 8, 2), instance_of(Array) ).and_return(shd2)
      StaffHolidayDay.should_receive(:new).with(Date.new(2013, 8, 3), instance_of(Array) ).and_return(shd3)
      StaffHolidayDay.should_receive(:new).with(Date.new(2013, 8, 4), instance_of(Array) ).and_return(shd4)

      result = calendar.send(:generate_days)
      result.should == [ shd1, shd2, shd3, shd4 ]
    end
  end



  describe 'private method #num_booked_holidays' do
    it 'should return the size of the booked holiday array' do
      calendar = StaffHolidayCalendar.new(Date.new(2013, 8, 1), Date.new(2013, 8, 4))
      calendar.instance_variable_set(:@booked_holiday_collection, [:a, :b, :c] )
      calendar.send(:num_booked_holidays).should == 3
    end
  end
end



