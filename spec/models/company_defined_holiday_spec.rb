# == Schema Information
#
# Table name: company_defined_holidays
#
#  id                     :integer          not null, primary key
#  name                   :string(255)
#  start_date             :date
#  end_date               :date
#  half_day_on_start_date :boolean
#  half_day_on_end_date   :boolean
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

require 'spec_helper'

describe CompanyDefinedHoliday do


  describe '.new' do
    it 'should raise error if first day is not a working day' do
      cdh = FactoryGirl.build :cdh_spans_weekend
      cdh.valid?.should be_false
      cdh.errors[:base].include?("Company Defined Holidays cannot include non-working days.  2013-05-18 is a non working day").should be_true
    end

    it 'should raise error is both first and last days are half days on the same day' do
      cdh = FactoryGirl.build :cdh_one_day_two_half_days
      cdh.valid?.should be_false
      cdh.errors[:base].include?("You cannot specify half day for both start and end when start and end dates are the same day").should be_true
    end

    it 'should raise error if overlapping with existing company defined holiday' do
      cdh1 = FactoryGirl.create :cdh_xmas_ny
      cdh2 = FactoryGirl.build :cdh_overlap
      cdh2.valid?.should be_false
      cdh2.errors[:base].include?("This holiday overlaps with an existing holiday").should be_true
    end

  end
  
  describe '#num_decadays' do
    let(:cdh)  { cdh = FactoryGirl.create :cdh_xmas_ny }
    
    it 'should return zero if the booked holiday ends before the company defined holiday starts' do
      bh =  FactoryGirl.create :booked_holiday, start_date: Date.new(2013, 12, 20), end_date: Date.new(2013, 12, 26)
      cdh.num_decadays(bh).should == 0
    end


    it 'should return zero if the booked holiday starts after the company defined holiday ends' do
      bh =  FactoryGirl.create :booked_holiday, start_date: Date.new(2014, 1, 1), end_date: Date.new(2014, 1, 15)
      cdh.num_decadays(bh).should == 0
    end


    it 'should return the number of decadays that the holiday overlaps when the holiday totally encompases the company holiday' do
      bh =  FactoryGirl.create :booked_holiday, start_date: Date.new(2013, 12, 24), end_date: Date.new(2014, 1, 2)
      cdh.num_decadays(bh).should == 20
    end


    it 'should return the number of decadays that the holiday overlaps when the holiday ends during the company defined holiday' do
      bh  = FactoryGirl.create :booked_holiday, start_date: Date.new(2013, 12, 24), end_date: Date.new(2013, 12, 30)
      cdh.num_decadays(bh).should == 10
    end


    it 'should return the number of decadays that the holiday overlaps when the holday starts dureing the company defined holiday' do
      bh  = FactoryGirl.create :booked_holiday, start_date: Date.new(2013, 12, 31), end_date: Date.new(2014, 1, 5)
      cdh.num_decadays(bh).should == 10
    end

    it 'should ignore half days on start for booked holidays that start before the company defined holiday' do
      bh  = FactoryGirl.create :booked_holiday, start_date: Date.new(2013, 12, 24), end_date: Date.new(2013, 12, 30), half_day_on_start_date: true
      cdh.num_decadays(bh).should == 10
    end

    it 'should subtract half a day when booked holiday has half day on start when the start date is during the company defined holiday' do
      bh  = FactoryGirl.create :booked_holiday, start_date: Date.new(2013, 12, 30), end_date: Date.new(2014, 1, 5), half_day_on_start_date: true
      cdh.num_decadays(bh).should == 15
    end

    it 'should subtract half a day when booked holiday has half day on end and the end date is during the company holiday' do
      bh  = FactoryGirl.create :booked_holiday, start_date: Date.new(2013, 12, 20), end_date: Date.new(2013, 12, 30), half_day_on_end_date: true
      cdh.num_decadays(bh).should == 5
    end


  end



end
