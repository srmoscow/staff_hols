# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  name                   :string(255)
#  is_admin               :boolean
#  manager_id             :integer
#  is_manager             :boolean
#  username               :string(255)
#

require 'spec_helper'

describe User do

  it { should validate_uniqueness_of :username }
  it { should validate_uniqueness_of :email }

  it { should validate_presence_of :username}
  it { should validate_presence_of :email}

  context 'scopes and associations' do 

    before(:each) do
      @a1 = FactoryGirl.create :admin
      @a2 = FactoryGirl.create :admin
      @m1 = FactoryGirl.create :manager
      @m2 = FactoryGirl.create :manager
      @u1 = FactoryGirl.create :user, :manager => @m1
      @u2 = FactoryGirl.create :user, :manager => @m1
    end

    it 'should return an array of staff reporting to the manager' do
      @m1.staff.should == [ @u1, @u2 ]
    end


    it 'should return the manager user object' do
      @u1.manager.should == @m1
    end

    it 'should return an array of all managers' do
      User.managers.should == [ @a1.manager, @a2.manager, @m1, @m2, ]
    end

  end


  context 'basic validation' do
    before(:each) do
      @attr = {
        :name => "Example User",
        :username => "user",
        :email => "user@example.com"
      }
    end

    it "should create a new instance given a valid attribute" do
      User.create!(@attr)
    end
  end


  describe '#roles' do
    specify { FactoryGirl.build(:user).roles.should == [] }
    specify { FactoryGirl.build(:admin).roles.should == [[1, 'Admin']] }
    specify { FactoryGirl.build(:manager).roles.should == [[2, 'Manager']] }
    specify { FactoryGirl.build(:admin_manager).roles.should == [[1, 'Admin'], [2, 'Manager']] }
  end


  describe '#role_names' do
    specify { FactoryGirl.build(:user).role_names.should == '' }
    specify { FactoryGirl.build(:admin).role_names.should == 'Admin' }
    specify { FactoryGirl.build(:manager).role_names.should == 'Manager' }
    specify { FactoryGirl.build(:admin_manager).role_names.should == 'Admin, Manager' }
  end


  describe '#allowance_for_current_year' do
    it 'should return the users allowance for the current year' do
      user      = FactoryGirl.create :user
      hy        = HolidayYear.find_or_create_now
      allowance = FactoryGirl.create :allowance, holiday_year_id: hy.id, user_id: user.id
      user.allowance_for_current_year.should == allowance
    end
  end


  describe '#allowance_for_year' do
    it 'should return the users allowance for the specified year' do
      user      = FactoryGirl.create :user
      hy        = FactoryGirl.create :holiday_year, year: 2015
      allowance = FactoryGirl.create :allowance, holiday_year_id: hy.id, user_id: user.id
      user.allowance_for_year(hy.id).should == allowance
    end
  end



  describe '#oustanding_requests' do
    it 'should get Booked Holiday to return an array of outstanding booked holidays' do
      manager = FactoryGirl.build :manager
      BookedHoliday.should_receive(:awaiting_approval_by).with(manager)
      manager.outstanding_requests
    end
  end


  describe '#update_roles' do
    it 'should set admin from params' do
      rec = FactoryGirl.build :manager
      rec.update_roles('is_admin' => true, 'is_manager' => false)
      rec.is_admin?.should be_true
      rec.is_manager?.should be_false
    end

    it 'should set is manager from params' do
      rec = FactoryGirl.build :admin
      rec.update_roles('is_admin' => true, 'is_manager' => true)
      rec.is_admin?.should be_true
      rec.is_manager?.should be_true
    end
  end


  describe '#create_allowance' do
    it 'should call create on Allowance' do
      user = FactoryGirl.create :user
      hy = FactoryGirl.create :holiday_year, year: 2013
      Allowance.should_receive(:create!).with(user_id: user.id, holiday_year_id: hy.id, num_days: 25, decadays_brought_forward: 30, carried_forward: 0, closed: false)
      user.create_allowance(2013, 25, 3)
    end
  end



  describe '#outstanding_requests_any' do
    it 'should return false if there are no outstanding requests' do
      manager = FactoryGirl.build :manager
      manager.should_receive(:outstanding_requests).and_return( [] )
      manager.has_outstanding_requests?.should be_false
    end

    it 'should return false if there are no outstanding requests' do
      manager = FactoryGirl.build :manager
      manager.should_receive(:outstanding_requests).and_return( [1, 2, 3] )
      manager.has_outstanding_requests?.should be_true
    end
  end


  describe '#viewable_staff' do
    it 'should return an empty collection for non-admin non-managers' do
      user = FactoryGirl.create :user
      user.viewable_staff.should == []
    end

    it 'should return all staff for admins' do
      # When I create some users and managers
      user_1 = FactoryGirl.create :user
      user_2 = FactoryGirl.create :user, manager: user_1.manager
      user_3 = FactoryGirl.create :user, manager: user_1.manager
      admin = FactoryGirl.create :admin

      # when I see viewable staff for an admin
      # It should return all staff except the admin ordered by name
      all_users = User.order('name')
      all_users.delete(admin)
      admin.viewable_staff.map(&:id).should == all_users.map(&:id)    # why doesn't admin.viewable_staff == all_orders?
    end

    it 'should return just reportees for non-admin managers' do
      # when I create some users and managers
      manager_1 = FactoryGirl.create :manager
      manager_2 = FactoryGirl.create :manager
      um1 = FactoryGirl.create :user, manager: manager_1
      um2 = FactoryGirl.create :user, manager: manager_1
      um3 = FactoryGirl.create :user, manager: manager_1
      um4 = FactoryGirl.create :user, manager: manager_2
      um5 = FactoryGirl.create :user, manager: manager_2

      manager_1.viewable_staff.should == [ um1, um2, um3 ]
    end

  end


end
