require 'spec_helper'

describe StaffHolidayDay do

  let(:booked_holiday_collections) {
      bh1 = FactoryGirl.create :booked_holiday, start_date: Date.new(2013,5,1), end_date: Date.new(2013,5,15) 
      bh2 = FactoryGirl.create :booked_holiday, start_date: Date.new(2013,5,5), end_date: Date.new(2013,5,20), half_day_on_start_date: true
      bh3 = FactoryGirl.create :booked_holiday, start_date: Date.new(2013,5,10), end_date: Date.new(2013,5,25), half_day_on_end_date: true
      bhs = [bh1, bh2, bh3]
      BookedHolidayCollectionForUser.group_by_user(bhs)
    }

  describe '.new' do

    it 'should populate chart_entries with one entry per booked holiday' do
      shd = StaffHolidayDay.new(Date.today, booked_holiday_collections)
      shd.chart_entries.size.should == 3
      shd.chart_entries.first.should be_instance_of(StaffHolidayChartEntry)
    end

    it 'should create_chart_entries_with only one_holiday for 2nd may' do
      shd = StaffHolidayDay.new(Date.new(2013,5,2), booked_holiday_collections)
      shd.chart_entries.map(&:on_holiday?).should == [true, false, false]
    end

    it 'should create chart entires with no half days for 2nd may' do
      shd = StaffHolidayDay.new(Date.new(2013,5,2), booked_holiday_collections)
      shd.chart_entries.map(&:half_day?).should == [false, false, false]
    end
  end


  describe '#date' do
    it 'should return two digit day number' do
      shd = StaffHolidayDay.new(Date.new(2013,5,8), booked_holiday_collections)
      shd.date.should == '08'
    end
  end
end

