
# == Schema Information
#
# Table name: holiday_years
#
#  id          :integer          not null, primary key
#  year        :integer
#  description :string(255)
#  start_date  :date
#  end_date    :date
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'spec_helper'

describe HolidayYear do


  describe 'validations' do
    it 'should validate correctly' do
      FactoryGirl.build(:holiday_year).should be_valid
      FactoryGirl.build(:holiday_year, :year => 2008).should_not be_valid
      FactoryGirl.build(:holiday_year, :year => 2100).should_not be_valid
      FactoryGirl.build(:holiday_year, :year => 2013.5).should_not be_valid
      FactoryGirl.build(:holiday_year, :year => nil, :start_date => nil, :end_date => nil).should_not be_valid
    end
  end

  describe '.now' do
    it 'should return the correct year record when the last day of the year is given' do
      FactoryGirl.create :holiday_year, year: 2012
      FactoryGirl.create :holiday_year, year: 2013
      FactoryGirl.create :holiday_year, year: 2014

      Date.stub(:today).and_return(Date.new(2013, 3, 31))
      HolidayYear.now.description.should == 'Year beginning April 2012'
    end

    it 'should return the correct year when a date in the middle of the year is given' do
      FactoryGirl.create :holiday_year, year: 2012
      FactoryGirl.create :holiday_year, year: 2013
      FactoryGirl.create :holiday_year, year: 2014

      Date.stub(:today).and_return(Date.new(2013, 6, 15))
      HolidayYear.now.description.should == 'Year beginning April 2013'
    end

    it 'should raise an error if there is no record in the database for today' do
      FactoryGirl.create :holiday_year, year: 2012
      FactoryGirl.create :holiday_year, year: 2014

      Date.stub(:today).and_return(Date.new(2013, 6, 15))
      expect { HolidayYear.now }.to raise_error RuntimeError, 'holiday_years table not populated with a record for 2013-06-15'
    end
  end


  describe '.for_date' do
    it 'should return the correct year record when the last day of the year is given' do
      FactoryGirl.create :holiday_year, year: 2012
      FactoryGirl.create :holiday_year, year: 2013
      FactoryGirl.create :holiday_year, year: 2014

      HolidayYear.for_date(Date.new(2013, 3, 31)).description.should == 'Year beginning April 2012'
    end

    it 'should return the correct year when a date in the middle of the year is given' do
      FactoryGirl.create :holiday_year, year: 2012
      FactoryGirl.create :holiday_year, year: 2013
      FactoryGirl.create :holiday_year, year: 2014

      HolidayYear.for_date(Date.new(2013, 6, 15)).description.should == 'Year beginning April 2013'
    end

    it 'should raise an error if there is no record in the database for today and raise_on_nil is true' do
      FactoryGirl.create :holiday_year, year: 2012
      FactoryGirl.create :holiday_year, year: 2014

      expect { HolidayYear.for_date(Date.new(2013, 6, 15)) }.to raise_error RuntimeError, 'holiday_years table not populated with a record for 2013-06-15'
    end


    it 'should return nil if there is no record in the database for today and raise_on_nil is false' do
      FactoryGirl.create :holiday_year, year: 2012
      FactoryGirl.create :holiday_year, year: 2014
      HolidayYear.for_date(Date.new(2013, 6, 15), false).should be_nil
    end

  end


  describe 'generate_next_year' do
    it 'should generate 1 year after the last year in the database' do
      FactoryGirl.create :holiday_year
      FactoryGirl.create :holiday_year, :year => 2013
      HolidayYear.count.should == 2

      HolidayYear.generate_next_year
      HolidayYear.count.should == 3
      hy = HolidayYear.last
      hy.year.should == 2014
      hy.description.should == 'Year Beginning April 2014'
      hy.start_date.should == Date.new(2014, 4, 1)
      hy.end_date.should == Date.new(2015, 3, 31)
    end
  end


  describe 'scope :year' do
    it 'should return the record for the named year' do
      FactoryGirl.create :holiday_year, :year => 2013
      FactoryGirl.create :holiday_year, :year => 2014
      hy = HolidayYear.by_year(2013)
      hy.year.should == 2013
    end
  end

  describe 'current' do
    let(:year) { FactoryGirl.create :holiday_year, :year => 2013 }

    it 'should return true if today is the year start date' do
      Date.stub(:today).and_return(year.start_date)
      year.current?.should be_true
    end

    it 'should return true if today is the year end date' do
      Date.stub(:today).and_return(year.end_date)
      year.current?.should be_true
    end

    it 'should return true if today is in between year start and end' do
      Date.stub(:today).and_return(year.start_date + 1.month)
      year.current?.should be_true
    end

    it 'should return false if today is before year start' do
      Date.stub(:today).and_return(year.start_date - 1.day)
      year.current?.should be_false
    end

    it 'should return false if today is after year end' do
      Date.stub(:today).and_return(year.end_date + 1.day)
      year.current?.should be_false
    end
  end

end




