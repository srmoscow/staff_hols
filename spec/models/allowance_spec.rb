# == Schema Information
#
# Table name: allowances
#
#  id                       :integer          not null, primary key
#  user_id                  :integer
#  holiday_year_id          :integer
#  num_days                 :integer
#  decadays_brought_forward :integer
#  carried_forward          :integer
#  closed                   :boolean
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#

require 'spec_helper'

describe Allowance do

  describe 'validations' do
    specify { FactoryGirl.build(:allowance).should be_valid }

    specify { FactoryGirl.build(:allowance, :user_id => 'abc').should_not be_valid }
    specify { FactoryGirl.build(:allowance, :holiday_year_id => 'abc').should_not be_valid }
    specify { FactoryGirl.build(:allowance, :num_days => 'abc').should_not be_valid }
    specify { FactoryGirl.build(:allowance, :decadays_brought_forward => 'abc').should_not be_valid }
    specify { FactoryGirl.build(:allowance, :carried_forward => 'abc').should_not be_valid }
    
    specify { FactoryGirl.build(:allowance, :user_id => nil).should_not be_valid }
    specify { FactoryGirl.build(:allowance, :holiday_year_id => nil).should_not be_valid }
    specify { FactoryGirl.build(:allowance, :num_days => nil).should_not be_valid }
    specify { FactoryGirl.build(:allowance, :decadays_brought_forward => nil).should_not be_valid }
    specify { FactoryGirl.build(:allowance, :carried_forward => nil).should_not be_valid }

    it 'should not allow duplicate allowances for the same user and year' do
      a1 = FactoryGirl.create :allowance
      a2 = FactoryGirl.build :allowance
      a2.should_not be_valid
      a2.errors[:user_id].should == ['Only one allowance record per user per holiday year is allowed']
    end

  end

  

  describe '.for_year_and_user' do
    it 'should return the record for the specified year and user' do
      a1 = FactoryGirl.create :allowance, user_id: 5, holiday_year_id: 12
      a2 = FactoryGirl.create :allowance, user_id: 5, holiday_year_id: 13
      a3 = FactoryGirl.create :allowance, user_id: 6, holiday_year_id: 12
      a4 = FactoryGirl.create :allowance, user_id: 6, holiday_year_id: 13

      Allowance.for_year_and_user(13, 5).should == a2
    end
  end

 
 


end
