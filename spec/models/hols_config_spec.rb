require 'spec_helper'

describe StaffHolsConfig do

  describe 'retrieving config variables with class methods' do
    specify { StaffHolsConfig.base_url.should == 'http://localhost:3000' }
    specify { StaffHolsConfig.from_address.should == 'TestHols <test_staff_hols@tagadab.com>'}
    specify { StaffHolsConfig.to_address.should == 'stephen@stephenrichards.eu'}
    specify { StaffHolsConfig.subject_prefix.should == '**** TEST **** '}
  end
end

