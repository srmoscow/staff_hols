#

require 'spec_helper'

describe Employee do

  describe '#authenticate_for_development' do
    it 'should return the user whose name is passed in as a param' do
      user = double(User)
      User.should_receive(:find_by_username).with('xxxx').and_return(user)
      Employee.authenticate_for_development('xxxx').should == user
    end
  end

end