
require 'spec_helper'

describe UserHolidaySet do

  context :predefined_years do 

    before(:each ) do
        @user_1 = FactoryGirl.create :user
        @user_2 = FactoryGirl.create :user
        @year_1 = FactoryGirl.create :holiday_year, year: 2013
        @year_2 = FactoryGirl.create :holiday_year, year: 2014
        @allowance11 = FactoryGirl.create :allowance, user_id: @user_1.id, holiday_year_id: @year_1.id
        @allowance21 = FactoryGirl.create :allowance, user_id: @user_2.id, holiday_year_id: @year_1.id
        @allowance12 = FactoryGirl.create :allowance, user_id: @user_1.id, holiday_year_id: @year_2.id
        @allowance22 = FactoryGirl.create :allowance, user_id: @user_2.id, holiday_year_id: @year_2.id
        @bh12a = FactoryGirl.create :five_day_booked_holiday_in_march_2014, user_id: @user_1.id, holiday_year_id: @year_2.id
        @bh12b = FactoryGirl.create :four_and_a_half_day_booked_holiday_in_june_2014, user_id: @user_1.id, holiday_year_id: @year_2.id
        @bh12c = FactoryGirl.create :three_day_unapproved_holiday_in_july_2014, user_id: @user_1.id, holiday_year_id: @year_2.id
        @bh12d = FactoryGirl.create :half_day_unapproved_booked_holiday_in_july_2014, user_id: @user_1.id, holiday_year_id: @year_2.id
    end


    describe '.new' do
      it 'should instantiate an object' do
        uhs = UserHolidaySet.new(@year_2.id, @user_1.id)
        uhs.user_id.should == @user_1.id
        uhs.user_name.should == @user_1.name
        uhs.year_description.should == @year_2.description
        uhs.allowance.should == @allowance12
        uhs.num_allowance_days.should == 20.0
        uhs.num_brought_forward_days.should == 5.0
        uhs.total_num_days.should == 25.0
        uhs.num_approved_holidays.should == 9.5
        uhs.num_unapproved_holidays.should == 3.5
        uhs.num_booked_holidays.should == 13.0
        uhs.num_days_available.should == 12.0
      end
    end


    describe '.new_for_next_year' do
      it 'should return nil if no HolidayYear record exists for next year' do
        Timecop.freeze(Time.local(2014, 6, 1, 12, 45, 0)) do
          UserHolidaySet.new_for_next_year(@user_1.id).should be_nil
        end
      end

      it 'should return nil if there is no allowance record for the holiday year / user' do
        # given a user for which there is no allowance
        user = FactoryGirl.create :user
        Timecop.freeze(Time.local(2013, 6, 1, 12, 45, 0)) do
          UserHolidaySet.new_for_next_year(user.id).should be_nil
        end
      end


      it 'should return a UHS for next year if a holiday year record exists for next year' do
        Timecop.freeze(Time.local(2013, 6, 1, 12, 45, 0)) do
          uhs = UserHolidaySet.new_for_next_year(@user_1.id)
          uhs.user_id.should == @user_1.id
          uhs.user_name.should == @user_1.name
          uhs.year_description.should == @year_2.description
          uhs.allowance.should == @allowance12
          uhs.num_allowance_days.should == 20.0
          uhs.num_brought_forward_days.should == 5.0
          uhs.total_num_days.should == 25.0
          uhs.num_approved_holidays.should == 9.5
          uhs.num_unapproved_holidays.should == 3.5
          uhs.num_booked_holidays.should == 13.0
          uhs.num_days_available.should == 12.0
        end
      end
    end




    describe '.approved_holidays' do
      it 'should return an array of approved followed by unapproved holidays' do
        uhs = UserHolidaySet.new(@year_2.id, @user_1.id)
        uhs.approved_holidays.should == [@bh12a, @bh12b]
      end
    end


    describe '.approved_or_unapproved_holidays' do
      it 'should return an array of approved followed by unapproved holidays' do
        uhs = UserHolidaySet.new(@year_2.id, @user_1.id)
        uhs.approved_or_unapproved_holidays.should == [@bh12a, @bh12b, @bh12c, @bh12d]
      end
    end


    describe '.all_viewable_for' do
      it 'should return an array of UserHolidaySets' do
        manager = @user_1.manager
        user_3 = FactoryGirl.create :user, manager: manager
        user_3_allowance = FactoryGirl.create :allowance, user_id: user_3.id, holiday_year_id: @year_1.id
        collection = UserHolidaySet.all_viewable_for(@year_1.id, manager)

        collection.should be_instance_of(Array)
        collection.size.should == 2
        collection.first.should be_instance_of(UserHolidaySet)
        collection.last.should be_instance_of(UserHolidaySet)
      end
    end

  end


  context :current_and_last_years do 

    let(:user)                { FactoryGirl.create :user }
    let(:current_year)        { FactoryGirl.create :current_holiday_year }
    let(:next_year)           { FactoryGirl.create :next_holiday_year}
    let(:current_allowance)   { FactoryGirl.create :allowance, user_id: user.id, holiday_year_id: current_year.id }
    let(:next_allowance)      { FactoryGirl.create :allowance, user_id: user.id, holiday_year_id: next_year.id}
    let(:cy_booked_holiday)   { FactoryGirl.create :booked_holiday, user: user, holiday_year: current_year, start_date: current_year.start_date, end_date: current_year.start_date + 5.days }
    let(:ny_booked_holiday)   { FactoryGirl.create :booked_holiday, user: user, holiday_year: next_year, start_date: next_year.start_date, end_date: next_year.start_date + 5.days}
    let(:cy_uhs)              { uhs = UserHolidaySet.new(current_year.id, user.id) }
    let(:ny_uhs)              { UserHolidaySet.new(next_year.id, user.id) }


    describe '#current_year' do
      
      it 'should return true for a UHS for the current year' do
        current_allowance
        cy_uhs.current_year?.should be_true
      end

      it 'should return false for a UHS for another year' do
        next_allowance
        ny_uhs.current_year?.should be_false
      end
    end


    describe '#hols_next_year' do
      it 'should return false if there are no hols booked next year' do
        current_allowance
        next_allowance
        cy_booked_holiday
        cy_uhs.hols_next_year?.should be_false
      end

      it 'shoudl return true if there are hols booked next year' do
        current_allowance
        next_allowance
        cy_booked_holiday
        ny_booked_holiday
        cy_uhs.hols_next_year?.should be_true
      end
    end

  end
end


