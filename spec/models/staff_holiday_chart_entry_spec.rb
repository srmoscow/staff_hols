require 'spec_helper'

describe StaffHolidayChartEntry do

  describe '.new' do
    it 'should set up instance variables when true' do
      date = double Date
      bh = double BookedHoliday
      bh.should_receive(:approved?).and_return(true)
      bh.should_receive(:holiday?).with(date).and_return(true)
      bh.should_receive(:half_day?).with(date).and_return(true)

      entry = StaffHolidayChartEntry.new(date, true, bh)
      entry.on_holiday?.should be_true
      entry.half_day?.should be_true
      entry.working_day?.should be_true
    end

    it 'should set up instance variables when true' do
      date = double Date
      bh = double BookedHoliday
      bh.should_receive(:approved?).and_return(true)
      bh.should_receive(:holiday?).with(date).and_return(false)
      bh.should_receive(:half_day?).with(date).and_return(false)

      entry = StaffHolidayChartEntry.new(date, false, bh)
      entry.on_holiday?.should be_false
      entry.half_day?.should be_false
      entry.working_day?.should be_false
    end
  end



  describe '#symbol' do

    let(:bh) {
      FactoryGirl.build :booked_holiday, :half_day_on_start_date => true 
    }

    it 'should return space if not a working day' do
      shce = StaffHolidayChartEntry.new(Date.new(2013, 3, 9), false, bh)    # Saturday
      shce.symbol.should == ' '
    end

    it 'should return h if half day holiday' do
      shce = StaffHolidayChartEntry.new(Date.new(2013, 3, 7), true, bh)   
      shce.symbol.should == 'h'
    end

    it 'should return H if full day holiday' do
      shce = StaffHolidayChartEntry.new(Date.new(2013, 3, 8), true, bh)   
      shce.symbol.should == 'H'
    end

    it 'should return W if a working day' do
      shce = StaffHolidayChartEntry.new(Date.new(2013, 3, 5), true, bh)   
      shce.symbol.should == 'W'
    end
  end

end


describe '#bgcolor' do
  let(:bh) {
      FactoryGirl.build :booked_holiday, :half_day_on_start_date => true 
    }

  it 'should return YELLOW if not a working day' do
    shce = StaffHolidayChartEntry.new(Date.new(2013, 3, 9), false, bh)    # Saturday
    shce.bgcolor.should == StaffHolidayChartEntry::YELLOW
  end

  it 'should return PINK if an approved booked holiday' do
    shce = StaffHolidayChartEntry.new(Date.new(2013, 3, 8), true, bh)    
    shce.bgcolor.should == StaffHolidayChartEntry::PINK
  end

  it 'should return LIGHT_PINK if unapproved booked holiday' do
    bh.should_receive(:approved?).and_return(false)
    shce = StaffHolidayChartEntry.new(Date.new(2013, 3, 8), true, bh)
    shce.bgcolor.should == StaffHolidayChartEntry::LIGHT_PINK
  end

  it 'should return GREEN for a workind day' do
    shce = StaffHolidayChartEntry.new(Date.new(2013, 3, 5), true, bh)   
    shce.bgcolor.should == StaffHolidayChartEntry::GREEN
  end


end
