# == Schema Information
#
# Table name: booked_holidays
#
#  id                     :integer          not null, primary key
#  user_id                :integer
#  holiday_year_id        :integer
#  start_date             :date
#  half_day_on_start_date :boolean
#  end_date               :date
#  half_day_on_end_date   :boolean
#  approver_id            :integer
#  date_approved          :date
#  decadays               :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  date_rejected          :date
#  active                 :boolean
#  deactivation_pending   :boolean          default(FALSE)
#



require 'spec_helper'

describe BookedHoliday do

  describe 'validations' do

    it 'should validate as expected' do

      FactoryGirl.build(:booked_holiday).should be_valid
      FactoryGirl.build(:unapproved_booked_holiday).should be_valid
    end

    specify { FactoryGirl.build(:booked_holiday, :approver_id => 'abc').should_not be_valid }
    specify { FactoryGirl.build(:booked_holiday, :user_id => nil).should_not be_valid }
    specify { FactoryGirl.build(:booked_holiday, :user_id => 'x').should_not be_valid }
    specify { FactoryGirl.build(:booked_holiday, :start_date => nil, :holiday_year => nil).should_not be_valid }
    specify { FactoryGirl.build(:booked_holiday, :end_date => nil).should_not be_valid }
    specify { FactoryGirl.build(:booked_holiday, :holiday_year_id => nil).should_not be_valid }
    specify { FactoryGirl.build(:booked_holiday, :holiday_year_id => 'x').should_not be_valid }
    specify { FactoryGirl.build(:booked_holiday, :active => nil).should_not be_valid }
    

    it 'should not allow half days on both start and end when start and end are the same day' do
      bh = FactoryGirl.build :booked_holiday, start_date: Date.today, end_date: Date.today, half_day_on_start_date: true, half_day_on_end_date: true
      bh.should_not be_valid
      bh.errors[:base].should == ["You cannot specify half day for both start and end when start and end dates are the same day"]
    end

    it 'should not allow a non-Date start date' do
      bh = FactoryGirl.build :booked_holiday, start_date: 'xxxx', holiday_year: nil
      bh.should_not be_valid
      bh.errors[:start_date].should == ["can't be blank", "Must be a Date object, is NilClass"]
    end

    it 'should not allow a non-Date end date'  do
      bh = FactoryGirl.build :booked_holiday, end_date: 'xxxx'
      bh.should_not be_valid
      bh.errors[:end_date].should == ["can't be blank", "Must be a Date object, is NilClass"]
    end


    it 'should not allow end date before start date' do
      bh = FactoryGirl.build :booked_holiday, start_date: '5/2/13', end_date: '3/2/13', holiday_year: nil
      bh.should_not be_valid
      bh.errors[:end_date].should == ['Cannot be before start date']
    end
  end



  describe 'check overlapping holidays' do
    it 'should not validate if the beginning of the holiday overlaps with an existing holiday' do
      bh1 = FactoryGirl.create :booked_holiday, start_date: Date.new(2013, 3, 11), end_date: Date.new(2013, 3, 15)
      bh2 = FactoryGirl.build :booked_holiday, start_date: Date.new(2013, 3, 14), end_date: Date.new(2013, 3, 20), user: bh1.user
      bh2.should_not be_valid
      bh2.errors[:base].first.should == 'This holiday overlaps with an existing holiday'
    end

    it 'should not validate if the beginning of the holiday starts on the same day as a previous holiday ends' do
      bh1 = FactoryGirl.create :booked_holiday, start_date: Date.new(2013, 3, 11), end_date: Date.new(2013, 3, 15)
      bh2 = FactoryGirl.build :booked_holiday, start_date: Date.new(2013, 3, 15), end_date: Date.new(2013, 3, 20), user: bh1.user
      bh2.should_not be_valid
      bh2.errors[:base].first.should == 'This holiday overlaps with an existing holiday'
    end



    it 'should not validate if the end of the holiday overlaps with an existing holiday' do
      bh1 = FactoryGirl.create :booked_holiday, start_date: Date.new(2013, 3, 11), end_date: Date.new(2013, 3, 15)
      bh2 = FactoryGirl.build :booked_holiday, start_date: Date.new(2013, 3, 8), end_date: Date.new(2013, 3, 12), user: bh1.user
      bh2.should_not be_valid
      bh2.errors[:base].first.should == 'This holiday overlaps with an existing holiday'
    end

    it 'should not validate if the entire holiday is encompased by an existing holiday' do
      bh1 = FactoryGirl.create :booked_holiday, start_date: Date.new(2013, 3, 11), end_date: Date.new(2013, 3, 15)
      bh2 = FactoryGirl.build :booked_holiday, start_date: Date.new(2013, 3, 12), end_date: Date.new(2013, 3, 13), user: bh1.user
      bh2.should_not be_valid
      bh2.errors[:base].first.should == 'This holiday overlaps with an existing holiday'
    end

    it 'should not validate if the holiday encompasses an existing holiday' do
      bh1 = FactoryGirl.create :booked_holiday, start_date: Date.new(2013, 3, 11), end_date: Date.new(2013, 3, 15)
      bh2 = FactoryGirl.build :booked_holiday, start_date: Date.new(2013, 3, 7), end_date: Date.new(2013, 3, 19), user: bh1.user
      bh2.should_not be_valid
      bh2.errors[:base].first.should == 'This holiday overlaps with an existing holiday'
    end

    it 'should validate if the holiday is between two existing holidays' do
      bh1 = FactoryGirl.create :booked_holiday, start_date: Date.new(2013, 3, 11), end_date: Date.new(2013, 3, 15)
      bh2 = FactoryGirl.create :booked_holiday, start_date: Date.new(2013, 4, 11), end_date: Date.new(2013, 4, 15), user: bh1.user
      
      bh3 = FactoryGirl.build :booked_holiday, start_date: Date.new(2013, 3, 21), end_date: Date.new(2013, 3, 28), user: bh1.user
      bh3.should be_valid
    end


    it 'should validate when updating an existing valid record' do
      bh1 = FactoryGirl.create :booked_holiday, start_date: Date.new(2013, 3, 11), end_date: Date.new(2013, 3, 15)
      bh2 = FactoryGirl.create :booked_holiday, start_date: Date.new(2013, 4, 11), end_date: Date.new(2013, 4, 15), user: bh1.user
      bh3 = FactoryGirl.create :booked_holiday, start_date: Date.new(2013, 3, 21), end_date: Date.new(2013, 3, 28), user: bh1.user

      bh3.end_date = Date.new(2013, 3, 29)
      bh3.should be_valid
    end

    it 'should be valid if overlapping with an inactive record' do
      bh1 = FactoryGirl.create :booked_holiday, start_date: Date.new(2013, 3, 11), end_date: Date.new(2013, 3, 15), active: false
      bh2 = FactoryGirl.build :booked_holiday, start_date: Date.new(2013, 3, 14), end_date: Date.new(2013, 3, 20), user: bh1.user
      bh2.should be_valid
    end

  end


  describe '#awaiting_approval_by' do
    it 'should return a list of BookedHoliday records awaiting approval by the named manager' do
      # Given a manager, some users, some of whom report to the manager, and approved and unapproved holiday requests for all
      hy2012                         = FactoryGirl.create :holiday_year
      manager                        = FactoryGirl.create :manager
      managed_user_1                 = FactoryGirl.create :user, manager_id: manager.id
      managed_user_2                 = FactoryGirl.create :user, manager_id: manager.id
      unmanaged_user_3               = FactoryGirl.create :user
      approved_holiday_for_user_1a   = FactoryGirl.create :booked_holiday, holiday_year_id: hy2012.id, user_id: managed_user_1.id, start_date: '1/1/2013', end_date: '10/1/2013'
      unapproved_holiday_for_user_1b = FactoryGirl.create :unapproved_booked_holiday, holiday_year_id: hy2012.id, user_id: managed_user_1.id, start_date: '1/2/2013', end_date: '10/2/2013'
      unapproved_holiday_for_user_2  = FactoryGirl.create :unapproved_booked_holiday, holiday_year_id: hy2012.id, user_id: managed_user_2.id, start_date: '1/2/2013', end_date: '10/2/2013'
      unapproved_holiday_for_user_4  = FactoryGirl.create :unapproved_booked_holiday, holiday_year_id: hy2012.id, user_id: unmanaged_user_3.id, start_date: '1/2/2013', end_date: '10/2/2013'

      # When I call awaiting_approval_by(manager)
      bhs = BookedHoliday.awaiting_approval_by(manager)

      # It should return just the unapproved holidays for the staff members reporting to the manager
      bhs.should == [ unapproved_holiday_for_user_1b, unapproved_holiday_for_user_2 ]
    end
  end



  describe 'finders' do

    def setup_hols
      # Given a number of records for various users and yeaR
      @user1 = FactoryGirl.create :user
      @user2 = FactoryGirl.create :user
      @bh1 = FactoryGirl.create :booked_holiday, start_date: '23/1/2013', end_date: '28/1/2013', user: @user1, holiday_year_id: 55
      @bh2 = FactoryGirl.create :booked_holiday, start_date: '23/2/2013', end_date: '28/2/2013', user: @user1, holiday_year_id: 55
      @bh3 = FactoryGirl.create :booked_holiday, start_date: '23/3/2013', end_date: '28/3/2013', user: @user1, holiday_year_id: 55
      @bh4 = FactoryGirl.create :booked_holiday, start_date: '23/1/2014', end_date: '28/1/2014', user: @user1, holiday_year_id: 56
      @bh5 = FactoryGirl.create :booked_holiday, start_date: '23/2/2014', end_date: '28/2/2014', user: @user1, holiday_year_id: 56
      @bh6 = FactoryGirl.create :booked_holiday, start_date: '23/3/2014', end_date: '28/3/2014', user: @user1, holiday_year_id: 56
      @bh7 = FactoryGirl.create :booked_holiday, start_date: '23/1/2013', end_date: '28/1/2013', user: @user2, holiday_year_id: 55
      @bh8 = FactoryGirl.create :booked_holiday, start_date: '23/2/2013', end_date: '28/2/2013', user: @user2, holiday_year_id: 55
      @bh9 = FactoryGirl.create :booked_holiday, start_date: '23/3/2013', end_date: '28/3/2013', user: @user2, holiday_year_id: 55
      @bhx = FactoryGirl.create :booked_holiday, start_date: '23/6/2013', end_date: '28/6/2013', user: @user2, holiday_year_id: 55, active: false

      # and some rejected ones
      @rej1 = FactoryGirl.create :rejected_booked_holiday, date_rejected: Date.today, start_date: '10/1/2013', end_date: '14/1/2013', user: @user1, holiday_year_id: 55
      @rej2 = FactoryGirl.create :rejected_booked_holiday, date_rejected: Date.today, start_date: '10/2/2013', end_date: '14/2/2013', user: @user1, holiday_year_id: 55
    end

    describe 'default scope' do
      it 'should only return active records on default scope' do
        setup_hols
        BookedHoliday.all.should == [@bh1, @bh2, @bh3, @bh4, @bh5, @bh6, @bh7, @bh8, @bh9, @rej1, @rej2]
      end

      it 'should return all when unscoped' do
        setup_hols
        BookedHoliday.unscoped.all.should == [@bh1, @bh2, @bh3, @bh4, @bh5, @bh6, @bh7, @bh8, @bh9, @bhx, @rej1, @rej2]
      end

    end


    describe '.all_future' do
      it 'should pick up all holidays that end today or in the future' do
        bh1 = FactoryGirl.create :booked_holiday, start_date: 10.days.ago.to_date,      end_date: 5.days.ago.to_date
        bh2 = FactoryGirl.create :booked_holiday, start_date: 5.days.ago.to_date,       end_date: Date.today.to_date
        bh3 = FactoryGirl.create :booked_holiday, start_date: 5.days.ago.to_date,       end_date: 2.days.from_now.to_date
        bh4 = FactoryGirl.create :booked_holiday, start_date: 5.days.from_now.to_date,  end_date: 10.days.from_now.to_date
        bh5 = FactoryGirl.create :booked_holiday, start_date: 5.days.from_now.to_date,  end_date: 10.days.from_now.to_date, active: false

        BookedHoliday.all_future.should == [bh2, bh3, bh4]
      end
    end


   

    describe '.by_year_and_user' do

      it 'should return an empty array if there are no matching records' do
        BookedHoliday.by_year_and_user(4, 44).should be_empty
      end

      it 'should return records in date order' do
        setup_hols
        BookedHoliday.by_year_and_user(56, @user1.id).should == [ @bh4, @bh5, @bh6 ]
      end
    end




    describe '.last_before_start' do
      it 'should return nil if there are no records before this date for this user' do
        setup_hols
        rec = FactoryGirl.build :booked_holiday, start_date: Date.new(2013, 1, 20), user: @user1
        BookedHoliday.last_before_start(rec).should be_nil
      end

      it 'should return a rejected record if I pass exclude_rejected = false' do
        setup_hols
        rec = FactoryGirl.build :booked_holiday, start_date: Date.new(2013, 1, 20), holiday_year_id: 55, user: @user1
        BookedHoliday.last_before_start(rec, :exclude_rejected => false).should == @rej1
      end



      it 'should return the last record starting before specified date if the specified date is in the middle' do
        setup_hols
        rec = FactoryGirl.build :rejected_booked_holiday, start_date: Date.new(2013, 3, 1), holiday_year_id: 55, user: @user1
        BookedHoliday.last_before_start(rec).should == @bh2
      end






      it 'should return the last record starting before specified date if the specified date is after all records' do
        setup_hols
        rec = FactoryGirl.build :booked_holiday, start_date: Date.new(2014, 8, 1), holiday_year_id: 56, user: @user1
        BookedHoliday.last_before_start(rec).should == @bh6
      end
    end


    describe '.last_before_end' do

      it 'should return nil if there are no records starting before the end of this record' do
        setup_hols
        rec = FactoryGirl.build :booked_holiday, end_date: Date.new(2013, 1, 20), user: @user1
        BookedHoliday.last_before_end(rec).should be_nil
      end

      it 'should return a rejected record if I pass exclude_rejected = false' do
        setup_hols
        rec = FactoryGirl.build :booked_holiday, end_date: Date.new(2013, 1, 20), holiday_year_id: 55, user: @user1
        BookedHoliday.last_before_end(rec, :exclude_rejected => false).should == @rej1
      end


      it 'should return the last record starting before end of the specified record if the record end date is in the middle' do
        setup_hols
        rec = FactoryGirl.build :booked_holiday, end_date: Date.new(2013, 3, 1), holiday_year_id: 55, user: @user1
        BookedHoliday.last_before_end(rec).should == @bh2
      end

      it 'should return the last record starting before specified date if the specified date is after all records' do
        setup_hols
        rec = FactoryGirl.build :booked_holiday, end_date: Date.new(2014, 8, 1), holiday_year_id: 56, user: @user1
        BookedHoliday.last_before_end(rec).should == @bh6
      end
    end
  end


  describe '.all_for_period' do
    it 'should return empty array if no holidays during period' do
      before  = FactoryGirl.create :booked_holiday, start_date: Date.new(2013,5,1), end_date: Date.new(2013,5,13)
      after   = FactoryGirl.create :booked_holiday, start_date: Date.new(2013,6,13), end_date: Date.new(2013,6,18)
      BookedHoliday.all_for_period(Date.new(2013,5,31), Date.new(2013,6,11)).should be_empty
    end


    it 'should return only holidays that fall within the period' do
      before          = FactoryGirl.create :booked_holiday, start_date: Date.new(2013,5,1), end_date: Date.new(2013,5,13)
      ending_on_start = FactoryGirl.create :booked_holiday, start_date: Date.new(2013,5,27), end_date: Date.new(2013,5,31)
      half_in         = FactoryGirl.create :booked_holiday, start_date: Date.new(2013,5,27), end_date: Date.new(2013,6,3)
      fully_in        = FactoryGirl.create :booked_holiday, start_date: Date.new(2013,6,1), end_date: Date.new(2013,6,8)
      half_out        = FactoryGirl.create :booked_holiday, start_date: Date.new(2013,6,8), end_date: Date.new(2013,6,16)
      starting_on_end = FactoryGirl.create :booked_holiday, start_date: Date.new(2013,6,11), end_date: Date.new(2013,6,16)
      after           = FactoryGirl.create :booked_holiday, start_date: Date.new(2013,6,13), end_date: Date.new(2013,6,18)

      expected = [ ending_on_start, half_in, fully_in, half_out, starting_on_end]
      BookedHoliday.all_for_period(Date.new(2013,5,31), Date.new(2013,6,11)).should == expected
    end
  end


  describe '#in_future?' do
    it { FactoryGirl.create(:booked_holiday, start_date: 4.days.ago.to_date, end_date: 3.days.ago.to_date).in_future?.should be_false }
    it { FactoryGirl.create(:booked_holiday, start_date: Date.today, end_date: 3.days.from_now.to_date).in_future?.should be_false }
    it { FactoryGirl.create(:booked_holiday, start_date: 1.day.from_now.to_date, end_date: 3.days.from_now.to_date).in_future?.should be_true }
  end



  describe '#in_past?' do
    it { FactoryGirl.create(:booked_holiday, start_date: 4.days.ago.to_date, end_date: 3.days.ago.to_date).in_past?.should be_true }
    it { FactoryGirl.create(:booked_holiday, start_date: Date.today, end_date: 3.days.from_now.to_date).in_past?.should be_true }
    it { FactoryGirl.create(:booked_holiday, start_date: 1.day.from_now.to_date, end_date: 3.days.from_now.to_date).in_past?.should be_false }
  end


  describe '#deletion_pending' do
    it { FactoryGirl.create(:booked_holiday, deactivation_pending: false).deletion_pending?.should be_false }
    it { FactoryGirl.create(:booked_holiday, deactivation_pending: true).deletion_pending?.should be_true }
  end



  describe '#holiday?' do
    let(:bh) { FactoryGirl.create :booked_holiday, start_date: Date.new(2013,5,1), end_date: Date.new(2013,5,13) }
    
    it { bh.holiday?(Date.new(2013,4,7)).should be_false }
    it { bh.holiday?(Date.new(2013,5,1)).should be_true }
    it { bh.holiday?(Date.new(2013,5,3)).should be_true }
    it { bh.holiday?(Date.new(2013,5,13)).should be_true }
    it { bh.holiday?(Date.new(2013,5,15)).should be_false }
  end


  describe '#half_day?' do
    let(:bh_half_day_start) { FactoryGirl.create :booked_holiday, start_date: Date.new(2013,5,1), half_day_on_start_date: true, end_date: Date.new(2013,5,13) }
    let(:bh_half_day_end)   { FactoryGirl.create :booked_holiday, start_date: Date.new(2013,5,1), half_day_on_end_date: true, end_date: Date.new(2013,5,13) }
    
    it { bh_half_day_start.half_day?(Date.new(2013,4,7)).should be_false }
    it { bh_half_day_start.half_day?(Date.new(2013,5,1)).should be_true }
    it { bh_half_day_start.half_day?(Date.new(2013,5,4)).should be_false }
    it { bh_half_day_start.half_day?(Date.new(2013,5,13)).should be_false }
    it { bh_half_day_start.half_day?(Date.new(2013,5,20)).should be_false }

    it { bh_half_day_end.half_day?(Date.new(2013,4,7)).should be_false }
    it { bh_half_day_end.half_day?(Date.new(2013,5,1)).should be_false }
    it { bh_half_day_end.half_day?(Date.new(2013,5,4)).should be_false }
    it { bh_half_day_end.half_day?(Date.new(2013,5,13)).should be_true }
    it { bh_half_day_end.half_day?(Date.new(2013,5,20)).should be_false }
  end



  describe '#approve!' do
    it 'should set the approver id and the date' do
      bh = FactoryGirl.create :unapproved_booked_holiday
      bh.approve!(bh.user.manager)
      bh.approver_id.should == bh.user.manager_id
      bh.date_approved.should == Date.today
    end
  end

  describe '#approved?' do
    it 'should return true if approved' do
      FactoryGirl.build(:booked_holiday).should be_approved
    end
    it 'should return false if not approved' do
      FactoryGirl.build(:unapproved_booked_holiday).should_not be_approved
    end
  end


  describe '#rejected?' do
    it 'should return true if rejected' do
      FactoryGirl.build(:rejected_booked_holiday).should be_rejected
    end
    it 'should return true if approved' do
      FactoryGirl.build(:booked_holiday).should_not be_rejected
    end
  end


  describe '#calendar_start_date' do
    it 'should return a date 9 days before the start of a 12-day holiday' do
      bh = FactoryGirl.create :booked_holiday, start_date: Date.new(2013,5,31), end_date: Date.new(2013,6,11)
      bh.start_date_for_calendar_display.should == Date.new(2013, 5, 22)
      bh.end_date_for_calendar_display.should == Date.new(2013, 6, 20)
    end
  end  





  describe 'before_save' do

    it 'should call set_num_days before saving a new record' do
      # Given a new record with no decadays, num_days
      bh = FactoryGirl.build :booked_holiday, decadays: nil


      # When I save it
      bh.save!

      # it should have populated decadays, and therefore num days
      bh.decadays.should == 50
      bh.num_days.should == 5.0
    end


    it 'should call set_num_days when re-saving an existing record' do
      # Given an existing record where
      bh = FactoryGirl.create :booked_holiday
      bh.start_date.should == Date.new(2013, 3, 7)
      bh.end_date.should == Date.new(2013, 3, 13)
      bh.decadays.should == 50
      bh.num_days.should == 5.0

      # When I update the start or end date, it should update the num_days
      bh.update_attribute :start_date, Date.new(2013, 3, 9)
      bh.decadays.should == 30
      bh.num_days.should == 3.0
    end
  end



  context 'company defined holidays' do
    it 'should not count company defined holidays when calculating number of days' do
      FactoryGirl.create :cdh_xmas_eve_half_day
      FactoryGirl.create :new_years_eve_half_day
      bh = FactoryGirl.create :booked_holiday, start_date: Date.new(2013, 12, 23), end_date: Date.new(2014, 1, 2)

      # Mon 23rd  Working day                   1.0
      # Tue 24th  Company Defined half day      1.5
      # Wed 25th  Public Holiday                1.5
      # Thu 26th  Public Holiday                1.5
      # Fri 27th  Working Day                   2.5
      # Sat 28th  Weekend                       2.5
      # Sun 29th  Weekend                       2.5
      # Mon 30th  WorkingDay                    3.5
      # Tue 31st  Company Defined half day      4.0
      # Wed 1st   Public Holiday                4.0
      # Thu 2nd   Working Day                   5.0
      bh.num_days.should == 5.0
    end
  end



  describe 'sort_key' do
    it 'should create a sort key from the user id and start date' do
      bh = FactoryGirl.create :booked_holiday, user_id: 888, start_date: Date.new(2013, 12, 23), end_date: Date.new(2014, 1, 2)
      bh.sort_key.should == '088820131223'
    end
  end



  describe 'sort' do
    it 'should sort accorging to user id and start date' do
      bh8801 = FactoryGirl.build :booked_holiday, user_id: 88, start_date: Date.new(2013, 11, 23), end_date: Date.new(2013, 11, 26)
      bh7702 = FactoryGirl.build :booked_holiday, user_id: 77, start_date: Date.new(2013, 12, 23), end_date: Date.new(2014, 1, 2)
      bh7701 = FactoryGirl.build :booked_holiday, user_id: 77, start_date: Date.new(2013, 11, 23), end_date: Date.new(2013, 11, 27)
      bh8802 = FactoryGirl.build :booked_holiday, user_id: 88, start_date: Date.new(2013, 12, 23), end_date: Date.new(2014, 1, 2)

      [bh8801, bh7702, bh7701, bh8802].sort.should == [bh7701, bh7702, bh8801, bh8802]
    end
  end


  describe '#deactivate!' do

    it 'should call deactivate_future_booked_holiday for future holidays' do
      bh = FactoryGirl.build :booked_holiday, start_date: 3.days.from_now.to_date, end_date: 5.days.from_now.to_date
      bh.should_receive(:deactivate_future_booked_holiday)
      bh.deactivate!
    end

    it 'should call request_past_booked_holiday_deactivation for holidays in the past' do
      bh = FactoryGirl.build :booked_holiday, start_date: 5.days.ago.to_date, end_date: 5.days.from_now.to_date
      bh.should_receive(:request_past_booked_holiday_deactivation)
      bh.deactivate!
    end
  end



  describe '#deactivate_future_booked_holiday' do
    it 'should set the holiday to false, and then send a mail and set the flash message' do
      # given a future booked holiday
      bh = FactoryGirl.build :booked_holiday, start_date:3.days.from_now.to_date, end_date: 10.days.from_now.to_date
      bh.active.should be_true

      mailer = double BookedHolidayMailer
      BookedHolidayMailer.should_receive(:future_booked_holiday_deleted_mail).with(bh).and_return(mailer)
      mailer.should_receive(:deliver)

      # wehen I deactivate it
      flash_message = bh.send(:deactivate_future_booked_holiday)

      # the record should be marked as inactive ....
      bh.reload.active.should be_false
      flash_message.should == "Booked Holiday #{bh.decorate} has been deleted and your manager notified"
    end
  end




  describe '#request_past_booked_holiday_deactivation' do
    it 'should set deactivation pending send a mail and return a flash message' do
      # given a future booked holiday
      bh = FactoryGirl.build :booked_holiday, start_date:13.days.ago.to_date, end_date: 6.days.ago.to_date
      bh.active.should be_true
      bh.deactivation_pending.should be_false

      mailer = double BookedHolidayMailer
      BookedHolidayMailer.should_receive(:past_booked_holiday_deleted_mail).with(bh).and_return(mailer)
      mailer.should_receive(:deliver)

      # wehen I deactivate it
      flash_message = bh.send(:request_past_booked_holiday_deactivation)

      # the record should be marked as inactive ....
      bh.reload.active.should be_true
      bh.deactivation_pending.should be_true
      flash_message.should == "Booked Holiday #{bh.decorate} is in the past, and your manager has been requested to delete this holiday."
    end
  end



  describe 'private method #one_day_holiday?' do
    it { FactoryGirl.build(:booked_holiday, start_date: 4.days.ago.to_date, end_date: 3.days.ago.to_date).send(:one_day_holiday?).should be_false }
    it { FactoryGirl.build(:booked_holiday, start_date: 4.days.ago.to_date, end_date: 4.days.ago.to_date).send(:one_day_holiday?).should be_true }
  end




  describe 'private method #one_half_day_holiday?' do
    let(:four_days_ago)   { 4.days.ago.to_date }
    let(:three_days_ago)  { 3.days.ago.to_date }

    it { FactoryGirl.build(:booked_holiday, start_date: four_days_ago, end_date: three_days_ago).send(:one_half_day_holiday?, four_days_ago).should be_false }
    it { FactoryGirl.build(:booked_holiday, start_date: four_days_ago, end_date: four_days_ago).send(:one_half_day_holiday?, four_days_ago).should be_false }
    it { FactoryGirl.build(:booked_holiday, start_date: four_days_ago, half_day_on_start_date: true, end_date: three_days_ago).send(:one_half_day_holiday?, four_days_ago).should be_false }
    it { FactoryGirl.build(:booked_holiday, start_date: four_days_ago, half_day_on_start_date: true, end_date: four_days_ago).send(:one_half_day_holiday?, four_days_ago).should be_true }
  end




  describe '#set_num_days' do

    it 'should count one day if start and end dates are the same' do
      bh = FactoryGirl.build :unapproved_booked_holiday, start_date: Date.new(2013,3,14), end_date: Date.new(2013,3,14)   # Thursday
      bh.set_num_days
      bh.num_days.should == 1.0
    end
 
    it 'should count half day if dates are same and start is half day' do
      bh = FactoryGirl.build :unapproved_booked_holiday, start_date: Date.new(2013,3,14), end_date: Date.new(2013,3,14), half_day_on_start_date: true   # Thursday
      bh.set_num_days
      bh.num_days.should == 0.5
    end     



    it 'should count two working days as two days' do
      bh = FactoryGirl.build :unapproved_booked_holiday, start_date: Date.new(2013,3,14), end_date: Date.new(2013,3,15)   # Thursday and Friday
      bh.set_num_days
      bh.num_days.should == 2.0
    end


    it 'should count two working days with half day on end date as 1.5 days' do
      bh = FactoryGirl.build :unapproved_booked_holiday, start_date: Date.new(2013,3,14), end_date: Date.new(2013,3,15), half_day_on_end_date: true    # Thursday and Friday
      bh.set_num_days
      bh.num_days.should == 1.5
    end


    it 'should count the whole week as 5 days' do
      bh = FactoryGirl.build :unapproved_booked_holiday, start_date: Date.new(2013,3,11), end_date: Date.new(2013,3,15)   # Monday to  Friday
      bh.set_num_days
      bh.num_days.should == 5.0
    end

    it 'should count the whole week with half days at either end as 4 days' do
      bh = FactoryGirl.build :unapproved_booked_holiday, 
                              start_date: Date.new(2013,3,11), 
                              end_date: Date.new(2013,3,15), 
                              half_day_on_start_date: true,
                              half_day_on_end_date: true   # Monday pm to  Friday am
      bh.set_num_days
      bh.num_days.should == 4.0
    end      



    it 'should not count the weekends' do
      bh = FactoryGirl.build :unapproved_booked_holiday, start_date: Date.new(2013,3,14), end_date: Date.new(2013,3,18)   # Thursday to Monday
      bh.set_num_days
      bh.num_days.should == 3.0
    end


    it 'should not count Easter' do 
      EnHolidayCalendar.calendar.holiday_name(Date.new(2013, 3, 29)).should == 'Good Friday'
      EnHolidayCalendar.calendar.holiday_name(Date.new(2013, 4, 1)).should == 'Easter Monday'
      bh = FactoryGirl.build :unapproved_booked_holiday, start_date: Date.new(2013,3,25), end_date: Date.new(2013,4,5)   # Two weeks Mon-Fri spanning Easter
      bh.set_num_days
      bh.num_days.should == 8.0
    end

    it 'should not count Spring Holiday' do
      EnHolidayCalendar.calendar.holiday_name(Date.new(2013, 5, 27)).should == 'Spring Bank Holiday'
      bh = FactoryGirl.build :unapproved_booked_holiday, start_date: Date.new(2013, 5, 23), end_date: Date.new(2013, 5, 29)   # Thursday to Wednesday spanning Spring Bank Holiday
      bh.set_num_days
      bh.num_days.should == 4.0
    end

    it 'should correctly calculate days when the start date is a weekend' do
      EnHolidayCalendar.calendar.weekend?(Date.new(2013,3,16)).should be_true   # Saturday
      EnHolidayCalendar.calendar.weekend?(Date.new(2013,3,17)).should be_true   # Sunday
      bh = FactoryGirl.build :unapproved_booked_holiday, start_date: Date.new(2013, 3, 16), end_date: Date.new(2013, 3, 18)   # Saturday to Monday
      bh.set_num_days
      bh.num_days.should == 1.0
    end

  end


  describe '#balance_if_approved' do
    it 'should return the balance of holidays in this year excluding rejected and inactive holidays' do
      # given a couple of holiday years
      year2013 = FactoryGirl.create :holiday_year, year: 2013
      year2014 = FactoryGirl.create :holiday_year, year: 2014

      # and a couple of users
      u1 = FactoryGirl.create :user
      u2 = FactoryGirl.create :user
      
      # and allowances
      allowance_u1_2013 = FactoryGirl.create :allowance, user_id: u1.id, holiday_year_id: year2013.id, num_days: 22, decadays_brought_forward: 35
      allowance_u2_2013 = FactoryGirl.create :allowance, user_id: u2.id, holiday_year_id: year2013.id, num_days: 20, decadays_brought_forward: 0
      allowance_u1_2014 = FactoryGirl.create :allowance, user_id: u1.id, holiday_year_id: year2014.id, num_days: 23, decadays_brought_forward: 45
      allowance_u2_2014 = FactoryGirl.create :allowance, user_id: u2.id, holiday_year_id: year2014.id, num_days: 21, decadays_brought_forward: 10

      # and some booked holidays, some of which are cancelled and some of which are rejected
      create_booked_holiday(user: u1, year: year2013, start_date: 20130501, num_days: 5, rejected: false, active: true)
      create_booked_holiday(user: u1, year: year2013, start_date: 20130801, num_days: 3, rejected: true, active: true)
      create_booked_holiday(user: u1, year: year2013, start_date: 20130810, num_days: 3, rejected: true, active: true)
      create_booked_holiday(user: u1, year: year2013, start_date: 20130930, num_days: 4, rejected: false, active: false)

      create_booked_holiday(user: u1, year: year2014, start_date: 20140501, num_days: 8, rejected: false, active: true)
      create_booked_holiday(user: u1, year: year2014, start_date: 20140801, num_days: 7, rejected: false, active: true)
      create_booked_holiday(user: u1, year: year2014, start_date: 20140820, num_days: 7, rejected: true, active: true)
      create_booked_holiday(user: u1, year: year2014, start_date: 20140930, num_days: 6, rejected: false, active: false)
      
      create_booked_holiday(user: u2, year: year2013, start_date: 20130501, num_days: 4, rejected: false, active: true)
      create_booked_holiday(user: u2, year: year2013, start_date: 20130801, num_days: 9, rejected: true, active: true)
      create_booked_holiday(user: u2, year: year2013, start_date: 20130810, num_days: 9, rejected: true, active: true)
      create_booked_holiday(user: u2, year: year2013, start_date: 20130930, num_days: 10, rejected: true, active: false)

      create_booked_holiday(user: u2, year: year2014, start_date: 20140501, num_days: 10, rejected: false, active: true)
      create_booked_holiday(user: u2, year: year2014, start_date: 20140801, num_days: 1, rejected: true, active: true)
      create_booked_holiday(user: u2, year: year2014, start_date: 20140810, num_days: 1, rejected: true, active: true)
      create_booked_holiday(user: u2, year: year2014, start_date: 20140930, num_days: 2, rejected: true, active: false)

      # When i create a new booked holiday in 2014 for user 1
      bh = create_booked_holiday(user: u1, year: year2014, start_date: 20131210, num_days: 5, rejected: false, active: true)

      # it should tell me that my balance if approved is n days
      # allowance of 23 days + 4.5 brought forward less 8 dasys and days of booked holidays, and this one 5 = 7.5
      bh.decadays_balance_if_approved.should == 75

    end






  end
end





def create_booked_holiday(opts)
      start_date_string = opts[:start_date].to_s
      start_date = Date.new(start_date_string[0..3].to_i, start_date_string[4..5].to_i, start_date_string[6..7].to_i)

      end_date = EnHolidayCalendar.calendar.working_days_after(start_date, opts[:num_days] - 1)

      FactoryGirl.create(:booked_holiday, 
            user:             opts[:user], 
            holiday_year:     opts[:year],
            start_date:       start_date,
            end_date:         end_date,
            date_approved:    opts[:rejected]  == false ? start_date - 4.days : nil,
            active:           opts[:active],
            date_rejected:    opts[:rejected]  == false ? nil : start_date - 4.days,
            holiday_year:     opts[:year])
    end
