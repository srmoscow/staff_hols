require 'spec_helper'

describe CalendarHelper do

  describe 'link_for_previous_month' do
    it { helper.link_for_previous_month(Date.new(2013, 8, 13)).should == "<a href=\"/calendar/show?date=2013-07-13\">July</a>" }
    it { helper.link_for_following_month(Date.new(2013, 8, 13)).should == "<a href=\"/calendar/show?date=2013-09-13\">September</a>" }
  end

end



