require 'spec_helper'

describe CalendarHelper do

  describe 'bootstrap_class_for flash_type' do

    it { helper.bootstrap_class_for(:success).should == 'alert-success' }
    it { helper.bootstrap_class_for(:error).should   == 'alert-error' }
    it { helper.bootstrap_class_for(:alert).should   == 'alert-block' }
    it { helper.bootstrap_class_for(:notice).should  == 'alert-info' }
    it { helper.bootstrap_class_for(:custom).should  == 'custom' }

  end


end

