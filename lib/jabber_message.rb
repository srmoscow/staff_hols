require 'eventmachine'
require 'blather'

CHATROOM = 'deploys@conference.openfire.tagadab.com'
USER = 'deploy@openfire.tagadab.com'
PASS = 'd3pl0c0n@1'

def jabber_message(room, message)
  $message = message
  trap(:INT) { EM.stop }
  trap(:TERM) { EM.stop }
  EM.run do
    Blather::Stream::Client.start(Class.new {
      attr_accessor :jid

      def post_init(stream, jid = nil)
        @stream = stream
        self.jid = jid

        pres = Blather::Stanza::Presence::Status.new
        pres.to = "#{CHATROOM}/#{USER}"
        pres.state = :chat
        @stream.send_data pres
        msg = Blather::Stanza::Message.new(CHATROOM, $message, :groupchat)
        @stream.send_data msg
        @stream.close_connection_after_writing
      end

      def receive_data(stanza)
        @stream.send_data stanza.reply!
      end

      def unbind
        EventMachine.stop
      end
    }.new, USER, PASS)
  end
end
