// Adds trim() function to String. Only needed for IE..
if(typeof String.prototype.trim !== 'function') 
{
  String.prototype.trim = function() 
	{
    return this.replace(/^\s+|\s+$/g, ''); 
  }
}

// If the string is an attr, if it's an array, it will be like "[1,2,3]".
// This will turn that into an array.
String.prototype.attrToArray = function()
{
	return eval(this.toString());
}

String.prototype.is_included_using_seperator = function(seperator, value)
{
	return this.split(seperator).include(value);
}