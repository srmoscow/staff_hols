
desc 'adds a holiday year record and allowances for the next holiday year'
task :add_year => :environment  do
	HolidayYearAdder.new.run
end