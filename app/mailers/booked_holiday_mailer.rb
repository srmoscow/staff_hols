class BookedHolidayMailer < ActionMailer::Base
  default from: "holiday_booking_system@tagadab.com"

  def holiday_request_mail(booked_holiday)
    @user           = booked_holiday.user
    @manager        = @user.manager
    @booked_holiday = booked_holiday.decorate
    addressee       = Rails.env.development? ? StaffHolsConfig.to_address : @manager.email
    subject         = "#{StaffHolsConfig.subject_prefix}Holiday Request from #{@user.name}"
    mail(:from => StaffHolsConfig.from_address, :to => addressee, :subject => subject)
  end



  def approve_holiday_request_mail(booked_holiday)
    @booked_holiday = booked_holiday.decorate
    @manager        = @booked_holiday.approver
    @user           = @booked_holiday.user
    @uhs            = UserHolidaySet.new(@booked_holiday.holiday_year.id, @booked_holiday.user.id)
    addressee       = Rails.env.development? ? StaffHolsConfig.to_address : @user.email
    subject         = "#{StaffHolsConfig.subject_prefix}Holiday Request Approved"
    mail(:from => StaffHolsConfig.from_address, :to => addressee, :subject => subject)
    end



  def reject_holiday_request_mail(booked_holiday)
    @booked_holiday = booked_holiday.decorate
    @manager        = booked_holiday.approver
    @user           = booked_holiday.user
    @uhs            = UserHolidaySet.new(@booked_holiday.holiday_year.id, @booked_holiday.user.id)
    addressee       = Rails.env.development? ? StaffHolsConfig.to_address : @user.email
    subject         = "#{StaffHolsConfig.subject_prefix}Holiday Request REJECTED"
    mail(:from => StaffHolsConfig.from_address, :to => addressee, :subject => subject)
  end




  def past_booked_holiday_deleted_mail(booked_holiday)
    @booked_holiday = booked_holiday.decorate
    @manager        = booked_holiday.approver
    @user           = booked_holiday.user
    @uhs            = UserHolidaySet.new(@booked_holiday.holiday_year.id, @booked_holiday.user.id)
    addressee       = Rails.env.development? ? StaffHolsConfig.to_address : @manager.email
    subject         = "#{StaffHolsConfig.subject_prefix} Deletion of Booked Holiday in the past requested"
    mail(:from => StaffHolsConfig.from_address, :to => addressee, :subject => subject)
  end



  def future_booked_holiday_deleted_mail(booked_holiday)
    @booked_holiday = booked_holiday.decorate
    @manager        = booked_holiday.approver
    @user           = booked_holiday.user
    @uhs            = UserHolidaySet.new(@booked_holiday.holiday_year.id, @booked_holiday.user.id)
    addressee       = Rails.env.development? ? StaffHolsConfig.to_address : @manager.email
    subject         = "#{StaffHolsConfig.subject_prefix} Deletion of future Booked Holiday"
    mail(:from => StaffHolsConfig.from_address, :to => addressee, :subject => subject)
  end



  def approve_deletion_request(booked_holiday, comment)
    @booked_holiday = booked_holiday.decorate
    @user           = booked_holiday.user
    @manager        = @user.manager
    @comment        = comment.html_safe
    @uhs            = UserHolidaySet.new(@booked_holiday.holiday_year.id, @booked_holiday.user.id)
    addressee       = Rails.env.development? ? StaffHolsConfig.to_address : @user.email
    subject         = "#{StaffHolsConfig.subject_prefix} Booked Holiday Deletion Request Approved"
    mail(:from => StaffHolsConfig.from_address, :to => addressee, :subject => subject)
  end


  def deny_deletion_request(booked_holiday, comment)
    @booked_holiday = booked_holiday.decorate
    @user           = booked_holiday.user
    @manager        = @user.manager
    @comment        = comment.html_safe
    @uhs            = UserHolidaySet.new(@booked_holiday.holiday_year.id, @user.id)
    addressee       = Rails.env.development? ? StaffHolsConfig.to_address : @user.email
    subject         = "#{StaffHolsConfig.subject_prefix} Booked Holiday Deletion Request Denied"
    mail(:from => StaffHolsConfig.from_address, :to => addressee, :subject => subject)
  end


end



