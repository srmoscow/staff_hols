# This class encapsulates all information about all staff holidays for a period of time
#
class StaffHolidayCalendar

  attr_reader :days, :booked_holiday_collection, :start_date


  def initialize(start_date, end_date)
    @start_date = start_date.to_date
    @end_date = end_date.to_date
    raise ArgumentError.new("End date must be after start date") unless @start_date < @end_date
    booked_hols = BookedHoliday.all_for_period(start_date, end_date)
    @booked_holiday_collection = BookedHolidayCollectionForUser.group_by_user(booked_hols.sort)
    @days  = generate_days
  end



  def self.new_for_month(date)
    cal = self.new(date.at_beginning_of_month, date.at_end_of_month)
  end



  def start_date_human
    @start_date.strftime('%a %d %b %Y')
  end

  def start_date_month_name
    @start_date.strftime('%B %Y')
  end


 

  private

  def generate_days
    days = []
    day = @start_date

    while day <= @end_date
      days << StaffHolidayDay.new(day, @booked_holiday_collection)
      day += 1
    end
    days
  end



  def num_booked_holidays
    @booked_holiday_collection.size
  end



end