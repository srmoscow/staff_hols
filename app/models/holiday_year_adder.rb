
# This class is responsible for adding a new holiday year, and is called from the
# rake task "add_year"
#

class HolidayYearAdder


  def initialize()
    @current_year = HolidayYear.now
    raise "No Holiday year found for today's date." if @current_year.nil?
    @next_year = HolidayYear.for_date(@current_year.end_date + 1.day)
    if @next_year.nil?
      @next_year = HolidayYear.find_or_create_for_date(@current_year.end_date + 1.day)
      puts "Creating Holiday year record for #{@next_year.description}"
    else
      puts "Using existing Holiday Year record for #{@next_year.description}"
    end
  end


  def run
    User.all.each { |user| generate_allowance(user) }
  end


  private

  # checks to see if an allowance exists for next year, and if not, copies the allownce from the previous year, adding one day if below 25 days.
  def generate_allowance(user)
    cy_allowance = user.allowance_for_year(@current_year.id)
    ny_allowance = user.allowance_for_year(@next_year.id)
    if ny_allowance.nil?
      ny_allowance = create_new_year_allowance(user, cy_allowance)
      puts "Created Allowance for #{user.name} for #{ny_allowance.num_days} days"
    else
      puts "Allowance already existed for #{user.name} for #{ny_allowance.num_days} days"
    end
  end



  def create_new_year_allowance(user, cy_allowance)
    allowance = Allowance.create!(
      :user_id                  => user.id,
      :holiday_year_id          => @next_year.id,
      :num_days                 => next_years_allowance(cy_allowance),
      :decadays_brought_forward => 0,
      :carried_forward          => 0,
      :closed                   => false)
  end


  def next_years_allowance(cy_allowance)
    if cy_allowance.num_days < 20 || cy_allowance.num_days >= 25
      cy_allowance.num_days
    else
      cy_allowance.num_days + 1
    end
  end


end

