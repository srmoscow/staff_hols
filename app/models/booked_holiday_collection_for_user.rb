class BookedHolidayCollectionForUser


  attr_reader :user_id, :user, :booked_holidays


  # instantiates 
  def initialize(user_id, booked_holidays)
    @user_id         = user_id
    @user            = User.find user_id
    @booked_holidays = booked_holidays
  end





  # instantiates an array of BookedHolidayCollectionForUser objects from a collection of booked holidays
  def self.group_by_user(booked_holidays)
    result_array = []
    booked_holidays.sort!
    user_ids = booked_holidays.map(&:user_id).uniq
    user_ids.each do |user_id|
      booked_holidays_for_user = booked_holidays.select { |bh| bh.user_id == user_id }
      result_array << BookedHolidayCollectionForUser.new(user_id, booked_holidays_for_user)
    end
    result_array
  end


  # returns the BookedHoliday object for the specified date, or a NullBookedHoliday if no booked holiday booked on this date for the user.
  def booked_holiday_for_date(date)
    result = NullBookedHoliday.new
    @booked_holidays.each do |bh|
      if bh.holiday?(date)
        result = bh
        break
      end
    end
    result 
  end


  def size
    @booked_holidays.size
  end





end