
#this class is repsonsible for closing the current holiday year and transferring the balances over tothe next holiday year.

class HolidayYearCloser


	def initialize(year_to_be_closed)
		@last_year = year_to_be_closed
		@next_year = HolidayYear.for_date(@last_year.end_date + 1.day)
		raise "No Holiday Year record found for year starting #{@last_year.end_date + 1.day} - run HolidayYearAdder" if @next_year.nil?
	end


	def run
		User.all.each { |user| copy_over_days(user) }
	end


	private

	def copy_over_days(user)
		uhs = UserHolidaySet.new(@last_year.id, user.id)
		
		allowance_cy = Allowance.for_year_and_user(@last_year.id, user.id)
		allowance_ny = Allowance.for_year_and_user(@next_year.id, user.id)
		raise "No Allowance  record found for user #{user.name} for year #{@next_year.description}- run HolidayYearAdder" if allowance_ny.nil?

		ActiveRecord::Base.transaction do
			allowance_ny.decadays_brought_forward = uhs.num_days_available * 10
			allowance_ny.save!

			allowance_cy.carried_forward = uhs.num_days_available * 10
			allowance_cy.closed = true
			allowance_cy.save!
		end
		puts "#{uhs.num_days_available} days carried over to next year for #{uhs.user.name}"

	end


end