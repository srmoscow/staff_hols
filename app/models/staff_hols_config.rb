
# This class allows the configuration variables to be accessed from the application.
#
# Config values are retrieved by calling them as class method on StaffHolsConfig, e.g.
#
#    StaffHolsConfig.base_url
#    StaffHolsConfig.from_address
#
# The first time a class method is called, an class variable will be created with all the values for the current Rails 
# Environment, and that will be used for all future calls.
#
class StaffHolsConfig

  @@config = nil

  
  def self.method_missing(meth, *params)
    if @@config.nil?
      @@config = OpenStruct.new(YAML.load_file("#{Rails.root}/config/staff_hols.yml")[Rails.env])
    end
    @@config.send(meth)
  end
end

