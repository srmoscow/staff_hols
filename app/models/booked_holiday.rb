# == Schema Information
#
# Table name: booked_holidays
#
#  id                     :integer          not null, primary key
#  user_id                :integer
#  holiday_year_id        :integer
#  start_date             :date
#  half_day_on_start_date :boolean
#  end_date               :date
#  half_day_on_end_date   :boolean
#  approver_id            :integer
#  date_approved          :date
#  decadays               :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  date_rejected          :date
#  active                 :boolean
#  deactivation_pending   :boolean          default(FALSE)
#

class BookedHoliday < ActiveRecord::Base

  include Comparable

  attr_accessible :approver_id, :date_approved, :end_date, :half_day_on_end_date, :half_day_on_start_date, :num_days, :start_date, :user_id, :holiday_year_id, :date_rejected, :active, :deactivation_pending

  default_scope   where(:active => true)

  belongs_to      :approver,
                  :class_name => User

  belongs_to      :holiday_year

  belongs_to      :user


  before_save     :set_num_days

  validates       :approver_id, numericality: true, allow_nil: true
  validates       :user_id, numericality: true
  validates       :start_date, :end_date, presence: true
  validates       :holiday_year_id, presence: true, numericality: true
  validates       :active, :inclusion => { :in => [true, false] }

  validates_with  PeriodValidator



  # Finders


  def self.all_future
    self.where('end_date >= ?', Date.today).order('start_date')
  end


  def self.all_for_period(start_date, end_date)
    self.where('(end_date >= ? AND end_date <= ?) OR (start_date >= ? AND start_date <= ?)', start_date, end_date, start_date, end_date).order('start_date')
  end

  

  def self.by_year_and_user(year_id, user_id)
    self.where('holiday_year_id = ? and user_id = ?', year_id, user_id).order('start_date')
  end


  # returns the last BookedHoliday record for the same user and holiday year whose start date is before the start date of <tt>rec</tt>.
  # @param [BookedHoliday] rec : A BookedHoliday record.  This method will return the last BookedHoliday records whose start date is before <tt>rec</tt>'s start date.
  # @param [Hash] options : An options hash which may contain the key :exclude_rejected, which can either be true or false.  If true, then rejected records will be
  # excluded from the search.  Default it true.
  def self.last_before_start(rec, options = {} )
    @exclude_rejected = options.has_key?(:exclude_rejected) ? options[:exclude_rejected] : true
    arel = BookedHoliday.where('holiday_year_id = ? and user_id = ? and start_date <= ?', rec.holiday_year_id, rec.user_id, rec.start_date)
    arel = rec.new_record? ? arel : arel.where('id != ?', rec.id)
    arel = @exclude_rejected == true ? arel.where('date_rejected is null') : arel
    arel.order('start_date DESC').first
  end

  # returns the last BookedHoliday record for the same user and holiday year whose end date is before the end_date of <tt>rec</tt>.
  # @param [BookedHoliday] rec : A BookedHoliday record.  This method will return the last BookedHoliday records whose end date is before <tt>rec</tt>'s start date.
  # @param [Hash] options : An options hash which may contain the key :exclude_rejected, which can either be true or false.  If true, then rejected records will be
  # excluded from the search.  Default it true. 
  def self.last_before_end(rec, options = {} )
    @exclude_rejected = options.has_key?(:exclude_rejected) ? options[:exclude_rejected] : true
    arel = BookedHoliday.where('holiday_year_id = ? and user_id = ? and start_date <= ?', rec.holiday_year_id, rec.user_id, rec.end_date)
    arel = rec.new_record? ? arel : arel.where('id != ?', rec.id)
    arel = @exclude_rejected == true ? arel.where('date_rejected is null') : arel
    arel.order('start_date DESC').first
  end




  # Returns an array of the booked holidays awaiting approval by the manager
  # @param [User] manager : The user object representing the managers whose staff's holiday requests we wish to return
  def self.awaiting_approval_by(manager) 
    staff_ids = manager.staff.map(&:id)
    self.where('approver_id is null and user_id in (?)', staff_ids)
  end



  # General methods

  # returns the balance of holidays for this user if this is approved
  def decadays_balance_if_approved
    # get all the other booked and waiting approval for this year and user excluding this one
    other_hols = self.class.where('holiday_year_id = ? and user_id = ? and id != ? and date_rejected is null and active = 1', self.holiday_year_id, self.user_id, self.id)
    other_hols_decadays = other_hols.sum(&:decadays)
    allowance = Allowance.for_year_and_user(self.holiday_year_id, self.user_id)
    set_num_days
    new_balance = allowance.total_decadays - other_hols_decadays - self.decadays
    new_balance
  end


  # Enables sorting by start_date within user_id
  def <=>(other)
    sort_key <=> other.sort_key
  end


  def sort_key
    sprintf('%04d%s', self.user_id, self.start_date.strftime('%Y%m%d'))
  end


  def in_future?
    self.start_date > Date.today
  end



  def in_past?
    !in_future?
  end


  def deletion_pending?
    self.deactivation_pending
  end



  # deactivates a booked holiday.  If the holiday is in the past, then a request is sent to the manager, and only the manager can de-activate it.
  def deactivate!
    flash = ''
    if in_future?
      flash = deactivate_future_booked_holiday
    else
      flash = request_past_booked_holiday_deactivation
    end
    flash
  end



  def deactivate_future_booked_holiday
    self.active = false
    self.save!
    BookedHolidayMailer.future_booked_holiday_deleted_mail(self).deliver
    "Booked Holiday #{self.decorate} has been deleted and your manager notified"
  end



  def request_past_booked_holiday_deactivation
    self.deactivation_pending = true
    self.save!
    BookedHolidayMailer.past_booked_holiday_deleted_mail(self).deliver
    "Booked Holiday #{self.decorate} is in the past, and your manager has been requested to delete this holiday."
  end



  def approved?
    !self.date_approved.nil?
  end

  def rejected?
    !self.date_rejected.nil?
  end

  def unapproved?
    self.approver_id.nil? && self.date_rejected.nil?
  end


 
  def num_days
    self.decadays.nil? ? 0.0 : self.decadays / 10.0
  end


  # returns two dates suitable for 
  def start_date_for_calendar_display
    self.start_date - calendar_leeway
  end



  def end_date_for_calendar_display
    self.end_date + calendar_leeway
  end




  def approve!(approver)
    update_attributes(approver_id: approver.id, date_approved: Date.today)
  end


  def reject!(approver)
    update_attributes(approver_id: approver.id, date_rejected: Date.today)
  end


  # returns true if the given date falls within the holiday
  def holiday?(date)
    return false if date < self.start_date || date > self.end_date
    return true
  end

  # returns true if the given date is a half day 
  def half_day?(date)
    return one_half_day_holiday?(date) if one_day_holiday? 
    if date == self.start_date
      return self.half_day_on_start_date
    elsif date == self.end_date
      return self.half_day_on_end_date
    end
    return false  
  end



  def self.existing_holiday_overlaps?(rec)
    start_overlaps = false
    end_overlaps = false

    # last_before_my_start = BookedHoliday.last_starting_before(rec.holiday_year_id, rec.user_id, rec.start_date)
    last_before_my_start = BookedHoliday.last_before_start(rec)
    last_before_my_end   = BookedHoliday.last_before_end(rec)
    
    start_overlaps = true if last_before_my_start && last_before_my_start.end_date >= rec.start_date
    end_overlaps = true if last_before_my_end && last_before_my_end.start_date >= rec.start_date

    result = start_overlaps || end_overlaps
    start_overlaps || end_overlaps
  end




  def set_num_days
    if self.active?
      calculator = WorkingDayCalculator.new(self)
      self.decadays = calculator.decadays
    else
      self.decadays = 0
    end
  end
    




  private


  def one_day_holiday?
    self.start_date == self.end_date
  end


  def one_half_day_holiday?(date)
    if self.start_date == date && self.end_date == date
      return self.half_day_on_end_date? || self.half_day_on_start_date
    end
    return false
  end



  # REturns the number of days to subtract from the start date and end to the end date when displaying a calendar
  def calendar_leeway
    num_calendar_days = (self.end_date - self.start_date).to_i
    (30 - num_calendar_days)/2
  end


end
