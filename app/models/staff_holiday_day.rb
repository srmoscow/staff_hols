# This class represents a day, and describes all that needs to be known about it
# in order to display who is on holiday or not for the day
#
class StaffHolidayDay


  attr_reader :chart_entries

  # Instantiate a StaffHolidayDay for a particular date from an array of BookedHolidayCollectionForUser objects
  def initialize(date, booked_holiday_collections)
    @date = date
    @working_day = EnHolidayCalendar.calendar.working_day?(@date)
    @chart_entries = populate_chart_entries(booked_holiday_collections)
  end


  def date
    @date.strftime('%d')
  end



  private


  # Here we populate the chart entries.  There will be one chart entry of each user, and each user has a BookedHolidayCollectionForUser object.
  # We iterate through the BookedHolidayCollectionForUser objects, and extract an appropriate BookedHoliday, creating an Null Booked Holiday for dates 
  # that aren't booked as holidays.
  def populate_chart_entries(booked_holiday_collections)
    chart_entries = []
    booked_holiday_collections.each { |bhc| chart_entries << StaffHolidayChartEntry.new(@date, @working_day, bhc.booked_holiday_for_date(@date)) }
    chart_entries
  end




end