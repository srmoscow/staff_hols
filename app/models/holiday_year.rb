# == Schema Information
#
# Table name: holiday_years
#
#  id          :integer          not null, primary key
#  year        :integer
#  description :string(255)
#  start_date  :date
#  end_date    :date
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class HolidayYear < ActiveRecord::Base
  attr_accessible :end_date, :year, :description, :start_date


  validates :year, :uniqueness => true, :presence => true, :numericality => {:only_integer => true, :greater_than => 2011, :less_than => 2030 }


  def self.generate_next_year
    year                  = self.last
    next_year             = self.new()
    next_year.year        = year.year + 1
    next_year.start_date  = year.start_date + 1.year
    next_year.end_date    = year.end_date + 1.year
    next_year.description = "Year Beginning April #{next_year.year}"
    next_year.save!
    next_year
  end


  # returns the year for today's date, or generates a new one if it doesn't exist
  def self.find_or_create_now
    hy = self.find_or_create_for_date(Date.today)
  end


  def self.find_or_create_for_date(date)
    rec = self.where('start_date <= ? and end_date >= ?', date, date).first
    if rec.nil?
      year            = date.month < 4 ? date.year - 1 : date.year
      rec             = self.new()
      rec.year        = year
      rec.start_date  = Date.new(year, 4, 1)
      rec.end_date    = rec.start_date + 1.year - 1.day
      rec.description = "Year Beginning April #{year}"
      rec.save!
    end
    rec
  end

  def current?
    Date.today >= start_date && Date.today <= end_date
  end


  def self.by_year(y)
    self.where(:year => y).first
  end
  


  def self.now
    for_date(Date.today)
  end


  def self.next_year
    for_date(Date.today + 1.year, false)
  end


  def next_year
    HolidayYear.for_date(self.end_date + 1.year, false)
  end


  def self.for_date(date, raise_on_nil = true)
    rec = self.where('start_date <= ? and end_date >= ?', date, date).first
    raise "holiday_years table not populated with a record for #{date.db}" if rec.nil? && raise_on_nil
    rec
  end



end
