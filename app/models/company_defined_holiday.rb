# == Schema Information
#
# Table name: company_defined_holidays
#
#  id                     :integer          not null, primary key
#  name                   :string(255)
#  start_date             :date
#  end_date               :date
#  half_day_on_start_date :boolean
#  half_day_on_end_date   :boolean
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class CompanyDefinedHoliday < ActiveRecord::Base
  attr_accessible :end_date, :half_day_on_end_date, :half_day_on_start_date, :name, :start_date


  validates_with  PeriodValidator

  validate :cannot_include_non_working_days



  def cannot_include_non_working_days
    day = self.start_date
    until day > self.end_date do
      unless EnHolidayCalendar.calendar.working_day?(day)
        errors[:base] << "Company Defined Holidays cannot include non-working days.  #{day} is a non working day"
      end
      day += 1
    end
  end







  # returns the number of company defined holidays (as decadays) during the specified booked holiday
  #
  def self.num_days_during(booked_holiday)
    num_decadays = 0
    self.all.each { |cdh| num_decadays += cdh.num_decadays(booked_holiday) }
    num_decadays
  end



  # Returns the number of dacadays that this company defined holiday overlaps with the booked holiday
  def num_decadays(booked_holiday)
   return 0 if booked_holiday.end_date < self.start_date or booked_holiday.start_date > self.end_date
   overlap_period = combined_period(booked_holiday, self)
   num_decadays = WorkingDayCalculator.new(overlap_period, :exclude_company_defined_holidays => true).decadays
  end


  def self.existing_holiday_overlaps?(rec)
    start_overlaps = false
    end_overlaps = false

    # last_before_my_start = BookedHoliday.last_starting_before(rec.holiday_year_id, rec.user_id, rec.start_date)
    last_before_my_start = self.last_before_start(rec)
    last_before_my_end   = self.last_before_end(rec)
    
    start_overlaps = true if last_before_my_start && last_before_my_start.end_date >= rec.start_date
    end_overlaps = true if last_before_my_end && last_before_my_end.start_date >= rec.start_date

    result = start_overlaps || end_overlaps
    start_overlaps || end_overlaps
  end


  # returns the last BookedHoliday record for the same user and holiday year whose start date is before the start date of <tt>rec</tt>.
  # @param [BookedHoliday] rec : A BookedHoliday record.  This method will return the last BookedHoliday records whose start date is before <tt>rec</tt>'s start date.
  #
  def self.last_before_start(rec)
    arel = self.where('start_date <= ?', rec.start_date)
    arel = rec.new_record? ? arel : arel.where('id != ?', rec.id)
    arel.order('start_date DESC').first
  end

  # returns the last BookedHoliday record for the same user and holiday year whose end date is before the end_date of <tt>rec</tt>.
  # @param [BookedHoliday] rec : A BookedHoliday record.  This method will return the last BookedHoliday records whose end date is before <tt>rec</tt>'s start date.
  #
  def self.last_before_end(rec, options = {} )
    arel = self.where('start_date <= ?', rec.end_date)
    arel = rec.new_record? ? arel : arel.where('id != ?', rec.id)
    arel.order('start_date DESC').first
  end






  private

  # returns an array of two dates, being the dates that the holidays overlap
  def combined_period(booked_holiday, company_defined_holiday)
    period = OpenStruct.new
    period.start_date = greatest_of(booked_holiday.start_date, company_defined_holiday.start_date)
    period.end_date = least_of(booked_holiday.end_date, company_defined_holiday.end_date)

    period.half_day_on_start_date = false
    period.half_day_on_end_date = false

    if (period.start_date == self.start_date && self.half_day_on_start_date?) || (period.start_date == booked_holiday.start_date && booked_holiday.half_day_on_start_date?)
      period.half_day_on_start_date = true
    end

    if (period.end_date == self.end_date && self.half_day_on_end_date?) || (period.end_date == booked_holiday.end_date && booked_holiday.half_day_on_end_date?)
      period.half_day_on_end_date = true
    end
    period
  end


  def least_of(date_1, date_2)
    date_1 < date_2 ? date_1 : date_2
  end


  def greatest_of(date_1, date_2)
    date_1 > date_2 ? date_1 : date_2
  end
 

end
