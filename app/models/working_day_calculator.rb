# This class calculates the number of working days between two dates

class WorkingDayCalculator

  # instantiates a calcutor with a period object.  The period object must respond to:
  # * start_date
  # * half_day_on_start_date
  # * end_date
  # * half_day_on_end_date
  #
  def initialize(period, options = {:exclude_company_defined_holidays => false})
    @period                 = period
    @exclude_cdh            = options[:exclude_company_defined_holidays]
    @start_date             = period.start_date
    @half_day_on_start_date = period.half_day_on_start_date
    @end_date               = period.end_date
    @half_day_on_end_date   = period.half_day_on_end_date
    @decadays               = 0
  end


  # returns the number of working days in decadays
  def decadays
    return 0 if @start_date.nil? || @end_date.nil?
    days = 1
    if @start_date != @end_date 
      days =  EnHolidayCalendar.calendar.count_working_days_inclusive(@start_date, @end_date)
    end
    @decadays = days * 10


    unless @exclude_cdh
      company_defined_decadays = CompanyDefinedHoliday.num_days_during(@period)
      @decadays -= company_defined_decadays
    end

    
    @decadays -= 5 if @half_day_on_start_date
    @decadays -= 5 if @half_day_on_end_date
    @decadays
  end
end

