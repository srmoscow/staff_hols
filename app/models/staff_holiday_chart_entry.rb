# This class represents one square on a staff holiday chart, and can either be a holiday for a particular staff member,
# a weekend or public holiday, or not a staff holiday.


class StaffHolidayChartEntry

  PINK       = 'FF8080'
  LIGHT_PINK = 'FFCCCC'
  GREEN      = 'E5FFCA'
  YELLOW     = 'FFFFCC'
  ORANGE     = 'FFB585'


  # instantiate StaffHolidayChartEntry for a specified date from a BookedHolidayCollectionForUser object
  def initialize(date, working_day, booked_holiday)
    @date        = date
    @approved    = booked_holiday.approved?
    @on_holiday  = booked_holiday.holiday?(date)
    @half_day    = booked_holiday.half_day?(date)
    @working_day = working_day
  end


  def on_holiday?
    @on_holiday
  end


  def half_day?
    @half_day
  end

  def working_day?
    @working_day
  end

  
  def symbol
    if !working_day?
      return ' '
    elsif half_day?
      return 'h'
    elsif on_holiday?
      return 'H'
    end 
    return 'W'
  end


  def bgcolor
    if !working_day?
      return YELLOW
    elsif on_holiday?
      if @approved
        return PINK
      else
        return LIGHT_PINK
      end
    end
    return GREEN
  end



end