# This class encapsulates all data about a user and his holidays and holiday entitlements for one year
#
class UserHolidaySet

  attr_reader :user, :year, :approved_holidays, :unapproved_holidays, :allowance, :rejected_holidays, 
              :next_year, :hols_next_year, :ny_approved_holidays, :ny_unapproved_holidays, :ny_allowance, :ny_rejected_holidays

  delegate :name, :to => :user, :prefix => true
  delegate :id, :to => :user, :prefix => true
  delegate :description, :to => :year, :prefix => true


  def initialize(year_id, user_id)
    @user                = User.find user_id
    @year                = year_id.nil? ? HolidayYear.now : HolidayYear.find(year_id)
    @next_year           = @year.next_year
    raise "No such holiday year record in the database for #{year_num}" if @year.nil?


    booked_holidays      = BookedHoliday.by_year_and_user(@year.id, @user.id).decorate
    @approved_holidays   = booked_holidays.select{ |bh| bh.approved? }
    @rejected_holidays   = booked_holidays.select{ |bh| bh.rejected? }
    @unapproved_holidays = booked_holidays.select{ |bh| bh.unapproved? }
    @allowance           = Allowance.for_year_and_user(@year.id, @user.id)
    raise "No Allowance record for user #{@user.name} for Holiday Year #{year.description}" if @allowance.nil?


    unless @next_year.nil?
      ny_booked_holidays      = BookedHoliday.by_year_and_user(@next_year.id, @user.id).decorate
      if ny_booked_holidays.empty?
        @hols_next_year = false
      else
        @ny_approved_holidays   = ny_booked_holidays.select{ |bh| bh.approved? }
        @ny_rejected_holidays   = ny_booked_holidays.select{ |bh| bh.rejected? }
        @ny_unapproved_holidays = ny_booked_holidays.select{ |bh| bh.unapproved? }
        @ny_allowance           = Allowance.for_year_and_user(@next_year.id, @user.id)
        @hols_next_year = true
      end
    end
  end


  def hols_next_year?
    @hols_next_year
  end



  def self.new_for_next_year(user_id)
    year = HolidayYear.next_year
    return nil if year.nil?
    return nil if Allowance.for_year_and_user(year.id, user_id).nil?
    self.new(year.id, user_id)
  end


  # returns an array of UserHolidaySets - one for each staff member viewable by user
  def self.all_viewable_for(year_id, user)
    result = []
    user.viewable_staff.each do |staff_member|
      result << UserHolidaySet.new(year_id, staff_member.id)
    end
    result
  end


  def current_year?
    @year.current?
  end



  def approved_or_unapproved_holidays
    @approved_holidays + @unapproved_holidays
  end



  # Returns the number of days in this years allaowance, returned as a float
  def num_allowance_days
    @allowance.num_days.to_f
  end

  # returns the number of days frought forward from the previous year
  def num_brought_forward_days
    @allowance.days_brought_forward
  end


  # Returns the total holiday entitlement for this year, i.e. allowance days + brought forward days.
  def total_num_days
    num_allowance_days + num_brought_forward_days
  end


  # Rturns the sum of all the approved holidays
  def num_approved_holidays
    @approved_holidays.sum(&:num_days)
  end


  # Returns the sum of all the unapproved holidays
  def num_unapproved_holidays
    @unapproved_holidays.sum(&:num_days)
  end


  # Returns the sum of approved and unapproved holidays
  def num_booked_holidays
    num_approved_holidays + num_unapproved_holidays
  end

  # Returns the number of holidays still available this year, i.e. the entitlement + brought forward days less approved and unapproved booked holidays.
  def num_days_available
    total_num_days - num_booked_holidays
  end



end