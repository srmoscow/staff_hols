# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  name                   :string(255)
#  is_admin               :boolean
#  manager_id             :integer
#  is_manager             :boolean
#  username               :string(255)
#

class User < ActiveRecord::Base
  
  # WE don't use rolify, just is_manager?, is_admin?
  # rolify

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :rememberable, :trackable,
         :concord_employee_authenticatable, authentication_keys: [:username]

  # Setup accessible (or protected) attributes for your model
  attr_accessible :role_ids, :as => :admin
  attr_accessible :name, :email, :username, :remember_me, :is_manager, :is_admin, :manager

  belongs_to    :manager, 
                :class_name => 'User', 
                :foreign_key => :manager_id

  has_many      :staff, 
                :class_name => 'User', 
                :foreign_key => :manager_id

  has_many      :allowances

  scope         :managers, where(:is_manager => true)

  scope         :all_except, ->(user) { where('id != ?', user.id).order('name') }

  validates :email, :username, uniqueness: true, presence: true

  def role_names
    roles.map{ |r| r.last }.join(', ')
  end

  def roles
    roles = []
    roles << [1, 'Admin'] if self.is_admin?
    roles << [2, 'Manager'] if self.is_manager?
    roles
  end



  def allowance_for_current_year
    allowance_for_year(HolidayYear.now.id)
  end



  def allowance_for_year(holiday_year_id)
    allowances.detect { |a| a.holiday_year_id == holiday_year_id }
  end



  #  Returns true if there are any BookedHolidays awaiting approval by this user
  def has_outstanding_requests?
    outstanding_requests.any?
  end

  # Returns an array of BookedHolidays awaiting approval by this user
  def outstanding_requests
    BookedHoliday.awaiting_approval_by(self)
  end

  # returns an array of users viewable by this user: reportees if manager, all staff if admin
  def viewable_staff
    if is_admin?
      User.all_except(self)
    else
      staff
    end
  end




  # updates the is_manager and is_admin attributes on the basis of params submitted from the form
  # Parameters: {"utf8"=>"✓", "authenticity_token"=>"3dJBTCyqztVsY0HCja+UZUaNYJbyX9DQuQpbglgYllM=", "user"=>{"is_admin"=>"0", "is_manager"=>"1"}, "commit"=>"Change Role", "id"=>"32"}
  def update_roles(params)
    raise 'Unexpected params - more than two elements' unless params.keys.size == 2
    raise "Unexpected params - keys: #{params.keys.inspect}" unless params.keys == ['is_admin', 'is_manager']
    self.update_attributes(params)
  end
  

  # Creates an Allowance record for the current user for the specified year
  # @param [Integer] year : The calendar year in which the holiday year starts, e.g. 2012, 2013
  # @param [Integer] num_days : The number of days in the allowance
  # @param [Integer] brought_forward : The number of days brought forward from the previous year
  def create_allowance(year, num_days, brought_forward = 0)
    hy = HolidayYear.by_year(year)
    Allowance.create!(
          user_id:                    self.id, 
          holiday_year_id:            hy.id, 
          num_days:                   num_days, 
          decadays_brought_forward:   (brought_forward * 10).to_i, 
          carried_forward:            0,
          closed:                     false)
  end

protected

  def password_required?
    puts "\n++++++++  ++++++ #{__FILE__}::#{__LINE__} +++++\n\n"
    Rails.env.development? ? false : true
  end

end
