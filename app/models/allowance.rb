# == Schema Information
#
# Table name: allowances
#
#  id                       :integer          not null, primary key
#  user_id                  :integer
#  holiday_year_id          :integer
#  num_days                 :integer
#  decadays_brought_forward :integer
#  carried_forward          :integer
#  closed                   :boolean
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#

class Allowance < ActiveRecord::Base

  attr_accessible :carried_forward, :holiday_year_id, :num_days, :user_id, :decadays_brought_forward, :closed

  validates :user_id,                     :presence => true, :numericality => true
  validates :holiday_year_id,             :presence => true, :numericality => true
  validates :num_days,                    :presence => true, :numericality => true
  validates :decadays_brought_forward,    :presence => true, :numericality => true
  validates :carried_forward,             :numericality => true
  validates :user_id,                     :uniqueness => {:scope => :holiday_year_id, :message => 'Only one allowance record per user per holiday year is allowed'}



  
  def self.for_year_and_user(year_id, user_id)
    self.where('holiday_year_id = ? and user_id = ?', year_id, user_id).first
  end


  def days_brought_forward
    (decadays_brought_forward/10.0).to_f
  end


  def total_decadays
    (num_days * 10) + decadays_brought_forward
  end


 
end



