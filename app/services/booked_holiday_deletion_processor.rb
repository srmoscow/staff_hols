class BookedHolidayDeletionProcessor

  def initialize(params)
    # sample params:
    # {"email_message"=>"This is the message I want to give.", "approve_deletion"=>"", "action"=>"confirm_deletion", "controller"=>"booked_holidays", "id"=>"18"}
    # or
    # {"email_message"=>"No way Jose!", "deny_deletion"=>"", "action"=>"confirm_deletion", "controller"=>"booked_holidays", "id"=>"18"}
    #
    @params         = params
    @booked_holiday = BookedHoliday.find(params[:id])
    @user_name      = @booked_holiday.user.name
    @comment        = params[:email_message].blank? ? nil : params[:email_message]
  end


  def run
    if @params.has_key?(:approve_deletion)
      approve_deletion
      mail_approval
      "Holiday has been deleted and a message sent to #{@user_name}"
    elsif @params.has_key?(:deny_deletion)
      deny_deletion
      mail_denial
      "Holiday Deletion Request has been denied, and a mail with your explanatory message, if any, has been sent to #{@user_name}"
    else
      raise "No approve_deletion or deny_deletion key in the params hash: #{@params.inspect}"
    end

  end



  private 

  def approve_deletion
    @booked_holiday.update_attributes(:deactivation_pending => false, :active => false)
  end


  def deny_deletion
    @booked_holiday.update_attributes(:deactivation_pending => false, :active => true)
  end


  def mail_approval
    BookedHolidayMailer.approve_deletion_request(@booked_holiday, @comment).deliver
  end


  def mail_denial
    BookedHolidayMailer.deny_deletion_request(@booked_holiday, @comment).deliver
  end

end