

# This class is responsible for taking the form params from the managers "Approve or Reject Holiday Requests" form and approve or reject them accordingly.
#
# Despite its name, it can reject as well as approve holidays.
#
class BookedHolidayApprover


  attr_reader :errors, :notes

  # Instantiates a BookedHolidayApprover object
  # @param [User] manager : The user that has approved or rejected the holiday requests
  # @param [HashWithIndifferentAccess] param : The form params passesd in, e.g.
  #   {"utf8"=>"✓",
  #    "authenticity_token"=>"elqKdYu9CdsPKqcIbgdZd5ImK84wyxrHdXI8NG9x5Do=",
  #    "approve_36"=>"1",
  #    "reject_38"=>"1",
  #    "action"=>"approve",
  #    "controller"=>"booked_holidays"}

  def initialize(manager, params)
    @errors = []
    @notes = []
    unless manager.is_manager? || manager.is_admin?
      @errors << "User #{manager.name} does not have rights to approve or reject holiday requests"
    end
    @manager = manager
    @approved_ids = []
    @rejected_ids = []
    extract_approved_and_rejected_ids(params)
    if @approved_ids.empty? && @rejected_ids.empty?
      @errors << 'No Booked Holidays were approved or rejected'
    end
   
  end



  def run
    raise "Cannot run a BookedHolidayApprover that is not valid" unless valid?
    approve
    reject
  end




  def valid?
    @errors.empty?
  end




  private



  def approve
    @approved_ids.each do |bhid|
      bh = BookedHoliday.find bhid
      bh.approve!(@manager)
      @notes << "Booked Holiday #{bh.decorate.to_s} for #{bh.user.name} has been approved and a mail has been sent."
      BookedHolidayMailer.approve_holiday_request_mail(bh).deliver
    end
  end

  def reject
    @rejected_ids.each do |bhid|
      bh = BookedHoliday.find bhid
      bh.reject!(@manager)
      @notes << "Booked Holiday #{bh.decorate.to_s} for #{bh.user.name} has been rejected and a mail has been sent."
      BookedHolidayMailer.reject_holiday_request_mail(bh).deliver
    end
  end



  def extract_approved_and_rejected_ids(params)
    params.each do |key, value|
      if key =~ /^approve_(\d+)$/  && value == "1"
        @approved_ids << $1.to_i
        next
      end
      if key =~ /^reject_(\d+)$/ && value == "1"
        @rejected_ids << $1.to_i
      end
    end
  end

end
