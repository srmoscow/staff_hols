
# This class accepts form parameters and instantiates a booked holiday object, or returns in an error state

class BookedHolidayCreator

  attr_reader :user, :booked_holidays, :errors, :notes
 
  # Typical Params:
  # {
  #   "utf8"=>"✓", 
  #   "authenticity_token"=>"vwVE8xwXU6IrlVjvKalQzRTAt/aYz037b8Iikg3UH5g=", 
  #   "start_date"=>"31/5/2013", 
  #   "half_day_on_start_date"=>"1", 
  #   "end_date"=>"11/6/2013", 
  #   "user_id"=>"29", 
  #   "action"=>"create", 
  #   "controller"=>"booked_holidays"}

  def initialize(params)
    @errors                 = []
    @notes                  = []
    @booked_holidays        = []
    @start_date             = Date.parse(params[:start_date]) unless params[:start_date].blank?
    @end_date               = Date.parse(params[:end_date]) unless params[:end_date].blank?
    @user_id                = params[:user_id].to_i if params.has_key?(:user_id)
    @user                   = User.find @user_id
    @half_day_on_start_date = params.has_key?(:half_day_on_start_date) ? true : false
    @half_day_on_end_date   = params.has_key?(:half_day_on_end_date) ? true : false
    
    @errors << "You must specify a start date." if @start_date.nil?
    @errors << "You must specify an end date." if @end_date.nil?

    if valid?
      @start_year = HolidayYear.for_date(@start_date)
      @end_year = HolidayYear.for_date(@end_date)
      if @start_year == @end_year
        @booked_holidays << create_booked_holiday(@start_year.id, @start_date, @half_day_on_start_date, @end_date, @half_day_on_end_date, @user_id)
      else
        details = split_dates
        @booked_holidays << create_booked_holiday(*details.first)
        @booked_holidays << create_booked_holiday(*details.last)
      end
      @notes << 'Holiday Request created.'
    end
  end

  def valid?
    @errors.empty?
  end



  private 


  def create_booked_holiday(holiday_year_id, start_date, half_day_on_start_date, end_date, half_day_on_end_date, user_id)
    begin
      bh = BookedHoliday.create!(
        :user_id                => user_id,
        :holiday_year_id        => holiday_year_id,
        :start_date             => start_date,
        :half_day_on_start_date => half_day_on_start_date,
        :end_date               => end_date,
        :active                 => true,
        :half_day_on_end_date   => half_day_on_end_date)
      BookedHolidayMailer.holiday_request_mail(bh).deliver
    rescue => err
      @errors << "#{err.class} #{err.message}"
    end
    bh
  end



  # This method is called when the to and from dates split holiday years, and returns two array, one for each year, each containing:
  # * holiday_year_id
  # * start_date
  # * half_day_on_start_date
  # * end_date
  # * half_day_on_end_date
  # * user_id
  def split_dates
    year_1 = [@start_year.id, @start_date, @half_day_on_start_date, @start_year.end_date, false, @user_id]
    year_2 = [@end_year.id, @end_year.start_date, false, @end_date, @half_day_on_end_date, @user_id]
    [year_1, year_2]
  end



  # def parse_date(date_string, field_name)
  #   # date_string =~ /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/
  #   date_string =~ /^([0-9]{2})\s([A-Z][A-z]+),\s([0-9]{4})$/


  #   begin
  #     date = Date.new($3.to_i, $2.to_i, $1.to_i)
  #   rescue ArgumentError => e
  #     @errors << "#{date_string} is not a valid date for #{field_name}."
  #   end
  #   date
  # end






end


