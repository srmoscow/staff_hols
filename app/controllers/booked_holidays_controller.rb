class BookedHolidaysController < ApplicationController
  
  load_resource :booked_holiday, :only => :index
  authorize_resource :booked_holiday
  


  def index
    @year            = HolidayYear.now
    @uhs_cy          = UserHolidaySet.new(@year.id, current_user.id)
    @uhs_ny          = UserHolidaySet.new_for_next_year(current_user.id)          # will be nil if no HolidayYear record exists for next year
    @months          = get_array_of_dates
    @booked_holidays = @booked_holidays.all_future
    @booked_holidays = @booked_holidays.decorate
  end


  def create
    @bhc = BookedHolidayCreator.new(params)
    if @bhc.valid?
      flash[:notice] = @bhc.notes
    else
      flash[:errors] = @bhc.errors
    end
  end


  def destroy
    bh = BookedHoliday.find(params[:id])
    msg = bh.deactivate!
    flash[:alert] = msg
    redirect_to action: :index
  end



  def approve
    approver = BookedHolidayApprover.new(current_user, params)
    approver.run if approver.valid?
    flash[:alert] = approver.notes
    redirect_to action: :index
  end


  def approve_deletion
    @booked_holiday = BookedHoliday.find(params[:id]).decorate
    @user = @booked_holiday.user
  end



  def confirm_deletion
    # sample params:
    # {"email_message"=>"This is the message I want ot give.", "approve_deletion"=>"", "action"=>"confirm_deletion", "controller"=>"booked_holidays", "id"=>"18"}
    # or
    # {"email_message"=>"No way Jose!", "deny_deletion"=>"", "action"=>"confirm_deletion", "controller"=>"booked_holidays", "id"=>"18"}
    #
    msg = BookedHolidayDeletionProcessor.new(params).run
    flash[:notice] = msg
    redirect_to booked_holidays_path
  end


  private

  def get_array_of_dates
    dates = []
    d = Date.today
    12.times do
      dates << d
      d += 1.month
    end
    dates
  end



end
