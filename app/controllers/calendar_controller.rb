class CalendarController < ApplicationController

  skip_authorization_check

  helper :calendar


  def show
    if params.has_key?(:id)
      @booked_holiday = BookedHoliday.find(params[:id]).decorate
      @calendar = StaffHolidayCalendar.new(@booked_holiday.start_date_for_calendar_display, @booked_holiday.end_date_for_calendar_display)
      @type = :booked_holiday
      @date = @booked_holiday.start_date.beginning_of_month
    elsif params.has_key?(:date)
      date = Date.parse(params[:date])
      @calendar = StaffHolidayCalendar.new_for_month(date)
      @type = :month
      @date = date.beginning_of_month
    else
      raise "No id or date parameter"
    end
  end

end
