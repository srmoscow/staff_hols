class AdminController < ApplicationController


  
  def index
    authorize! :index, :admin, :message => 'Not authorized as an administrator.'
    @year = HolidayYear.now
    @user_holiday_sets = UserHolidaySet.all_viewable_for(@year.id, current_user)
  end



end
