class ApplicationController < ActionController::Base

  check_authorization :unless => :devise_controller?

  rescue_from CanCan::AccessDenied do |exception|
    if Rails.env == "development"
      raise CanCan::AccessDenied.new("Do not have permission to #{exception.action} #{exception.subject}", exception.action, exception.subject)
    end
    
    if current_user
      render :file => "#{Rails.root}/public/403", :status => 403, :layout => false, formats: [:html]
    else
      #can't redirect logged in users here, because it will then redirect them somewhere else and there is the risk of a loop
      redirect_to new_user_session_path, :alert => "You need to log in before you can view that page."
    end
  end
  

  before_filter :authorize_miniprofiler
  def authorize_miniprofiler
    if defined?(Rack::MiniProfiler) && (Rails.env.development? )
      Rack::MiniProfiler.authorize_request
    end
  end



  protect_from_forgery

  before_filter :authenticate_user!
  before_filter :determine_revision

  # rescue_from CanCan::AccessDenied do |exception|
  #   redirect_to root_path, :alert => exception.message
  # end


  # Returns the template filename for the current controller / action
  #
  def template_filename(action)
    @@template_filenames = {} unless defined?(@@template_filenames)
    if @@template_filenames[self.class.to_s].nil?
      @@template_filenames[self.class.to_s] = {}
    end

    if @@template_filenames[self.class.to_s][action].nil?    
      pattern = "#{Rails.root}/app/views/#{self.class.to_s.underscore.sub(/_controller$/, '')}/#{action}*"
      @@template_filenames[self.class.to_s][action] = Dir[pattern].first
    end
    @@template_filenames[self.class.to_s][action]
  end



  private 

  def determine_revision
    if File.exist?("#{Rails.root}/REVISION")
      @revision = File.open("#{Rails.root}/REVISION") do |fp|
        fp.readline[0..7]
      end
    else
      @revision = 'Unversioned'
    end
  end


end




