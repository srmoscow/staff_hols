module ApplicationHelper

  def display_base_errors resource
    return '' if (resource.errors.empty?) or (resource.errors[:base].empty?)
    messages = resource.errors[:base].map { |msg| content_tag(:p, msg) }.join
    html = <<-HTML
    <div class="alert alert-error alert-block">
      <button type="button" class="close" data-dismiss="alert">&#215;</button>
      #{messages}
    </div>
    HTML
    html.html_safe
  end


  # returns the appropriate flash class depending on the type of flash message
  def flash_class(name)
    case name
    when :notice
      'alert alert-success'
    when :errors
      'alert alert-error'
    else
      'alert alert-warning'
    end
  end



end
