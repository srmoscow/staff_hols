module CalendarHelper


  def link_for_previous_month(date)
    previous_date = date - 1.month
    link_to previous_date.strftime('%B'), calendar_show_path(:date => previous_date)
  end


  def link_for_following_month(date)
    following_date = date + 1.month
    link_to following_date.strftime('%B'), calendar_show_path(:date => following_date)
  end
end
