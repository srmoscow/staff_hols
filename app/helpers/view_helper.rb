module ViewHelper
  
  # def title(page_title, tag=:h1, options={})
  #   if content_for?(:title)
  #     content_for(:title, " - "+page_title)
  #   else
  #     content_for(:title, t('vdc_header'))
  #     content_for(:title, " - "+page_title)
  #   end
  #   return content_tag(tag, page_title, options)
  # end
  
  # def top_bar_list_item_and_link_for(title, path, controller_base)
  #   capture_haml do
  #     haml_tag :li, class:('active' if params[:controller] =~ /\A#{controller_base}(\/|\z)/) do
  #       haml_tag :a, title, href:path
  #     end
  #   end
  # end
  
  def bootstrap_class_for flash_type
    case flash_type
    when :success
      "alert-success"
    when :error
      "alert-error"
    when :alert
      "alert-block"
    when :notice
      "alert-info"
    else
      flash_type.to_s
    end
  end

  # Renders a button group with buttons as defined in the options param.
  # Expects some kind of form input nested within this call.
  # Options should be a hash with each pair in the form of 'Text'=>'Action' where text is
  # the button text disabled to the user, and action controlls what will be submitted to the form:
  #   string - hide the input from the user, form will submit the action value.
  #   false  - hide and disable the input, nothing will submit with form.
  #   nil    - show the user the input, form will submit whatever they enter.
  def default_or_custom_input(options, params, &block)
    render '/shared/default_or_custom_input', options: options, default: params[:default], disabled: params[:disabled], block: capture(&block)
  end

end
