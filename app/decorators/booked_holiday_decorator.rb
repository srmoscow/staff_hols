class BookedHolidayDecorator < Draper::Decorator

  include Rails.application.routes.url_helpers
  delegate_all

  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       source.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end




  def to_s
    "From #{model.start_date.dmy} to #{model.end_date.dmy} (#{model.num_days} working days)"
  end



  def approval_status
    if model.approved?
      model.approver.name
    elsif model.rejected?
      'Rejected'
    else
      'Awaiting Approval'
    end
  end



  def start_date_db
    db(model.start_date)
  end


  def end_date_db
    db(model.end_date)
  end


  def start_date_human
    human(model.start_date)
  end


  def end_date_human
    human(model.end_date)
  end



  def deletion_confirmation_message
    model.in_future? ? future_deletion_confirmation_message : past_deletion_confirmation_message
  end


  def balance_if_approved
    model.decadays_balance_if_approved.to_f / 10
  end



  private

  def future_deletion_confirmation_message
    "Are you sure you want to delete the future booked holiday #{to_s}?"
  end


  def past_deletion_confirmation_message
    "The booked holiday #{to_s} is in the past, and you cannot delete it. However, if you press OK, a message will be sent to your manager asking him to delete it.  Go ahead?"
  end



  def db(date)
    date.strftime('%Y-%m-%d')
  end

  def human(date)
    date.strftime('%a %d %b %Y')
  end


 

end
