

class PeriodValidator < ActiveModel::Validator


  def validate(record)
    dates_must_be_date_objects(record)
    end_date_cannot_be_before_start_date(record)
    start_date_and_end_date_cannot_both_be_half_days_on_the_same_day(record)
    holiday_cannot_overlap_with_existing_holiday(record)
  end





  def holiday_cannot_overlap_with_existing_holiday(record)
    if record.class.existing_holiday_overlaps?(record)
      record.errors.add(:base, "This holiday overlaps with an existing holiday")
    end
  end


  def start_date_and_end_date_cannot_both_be_half_days_on_the_same_day(record)
    if record.start_date == record.end_date && record.half_day_on_start_date == true && record.half_day_on_end_date == true
      record.errors.add(:base, "You cannot specify half day for both start and end when start and end dates are the same day")
    end
  end



  def dates_must_be_date_objects(record)
    unless record.start_date.is_a?(Date) 
      record.errors.add(:start_date, "Must be a Date object, is #{record.start_date.class}")
    end
    unless record.end_date.is_a?(Date) 
      record.errors.add(:end_date, "Must be a Date object, is #{record.end_date.class}")
    end
  end


  def end_date_cannot_be_before_start_date(record)
    # Don't do this validation if we've already found errors on dates - they're probably not date objects
    if (record.errors[:start_date].empty? && record.errors[:end_date].empty?)
      if record.end_date < record.start_date
        record.errors.add(:end_date, "Cannot be before start date")
      end
    end
  end
end
