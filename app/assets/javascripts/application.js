// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//= require jquery
//= require jquery.ui.slider
//= require jquery.ui.datepicker
//= require jquery.ui.sortable
//= require jquery_ui_blockui
//= require jquery_ujs
//= require twitter/bootstrap
//= require bootstrap-dropdown
//= require bootstrap-modal
//= require bootstrap-modalmanager
//= require bootstrap-editable-inline
//= require bootstrap-datetimepicker
//= require bootstrap-switch
//= require_tree ./application
//= require class_extensions

guard = function( method ) {
  try { method() } catch(e) { console.error(e) }
}

// basic page load binding things that don't really warrant their own js file
$(document).bind('_page_load', function() {
  $('body').addClass('js-enabled')

  $('.js-on').show()
  $('.js-off').hide()
  $('.js-off').find('input, select, textarea, button').each(function(){
    $(this).attr('disabled','disabled')
  })

  guard(function(){ $(".popover").remove(); $('.has_popover').popover() })
  guard(function(){ $('.cost-button').tooltip({placement:'bottom'}) })

  $('.dropdown-toggle').dropdown()
})

$(document).ready(function(){
  $(document).trigger('_page_load')
})
