class BookedHolidayActionCell < Cell::Rails

  include ActionController::UrlFor
  include ActionView::Helpers::UrlHelper

  def show(args)
    booked_holiday = args[:booked_holiday]
    if booked_holiday.deletion_pending?
      @action_string = "Deletion Pending Manager Approval"
    else
      view_link = link_to 'View', calendar_show_path(:id => booked_holiday)
      
      @action_string = "#{view_link}&nbsp;#{delete_link(booked_holiday)}".html_safe
    end

    
    render  # renders app/cells/booked_holiday_action_cell/show.html.haml
  end

  private

  def delete_link(booked_holiday)
    conf_message = "Are you sure you want to delete the holiday #{booked_holiday}}?"
    if booked_holiday.in_past?
      conf_message = "The holiday #{booked_holiday} is in the past, and will have to be approved by your manager. Do you want to go ahead and send the request?"
    end
    link_to 'Delete', booked_holiday, :method => :delete, :confirm => conf_message
  end

end
